package com.pajk.justice.openapi.biz.request.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet.FeedbackResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class FeedbackRequest implements IRequest<FeedbackResult> {


    String apiId = "149fc5e06674b978b16e6911d0b85b86";

    String apiGroup = "shennong";
    String apiName = "feedbackResult";


    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private Long storeId;

    /**
     * 外部门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private String outStoreId;

    /**
     * 订单ID
     */
    private String tradeId;

    /**
     * 按SKU维度的商品履约情况
     *
     * 例如 skuId1:successCnt1;skuId2:successCnt2
     */
    private String skuParam;

    /**
     * 按OUT SKU维度的商品履约情况
     *
     * 例如 outSkuId1:successCnt1;outSkuId2:successCnt2
     */
    private String outSkuParam;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {

        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {

        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getSkuParam() {
        return skuParam;
    }

    public void setSkuParam(String skuParam) {
        this.skuParam = skuParam;
    }

    public String getOutSkuParam() {
        return outSkuParam;
    }

    public void setOutSkuParam(String outSkuParam) {
        this.outSkuParam = outSkuParam;
    }




    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("storeId", storeId);
        map.put("outStoreId", outStoreId);
        map.put("tradeId", tradeId);
        map.put("skuParam", skuParam);
        map.put("outSkuParam", outSkuParam);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<FeedbackResult> getClassName() {
        return FeedbackResult.class;
    }
}
