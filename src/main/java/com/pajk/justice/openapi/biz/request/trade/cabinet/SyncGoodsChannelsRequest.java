package com.pajk.justice.openapi.biz.request.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet.SyncGoodsChannelsResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SyncGoodsChannelsRequest implements IRequest<SyncGoodsChannelsResult> {

    private String apiId = "fe6e4be98335142ed3b5d6090083fc2d";
    private String apiGroup = "shennong";
    private String apiName = "syncGoodsChannels";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * 货道列表
     */
    private List<GoodsChannel> goodsChannelParams;

    /**
     * 货架
     */
    private String goodsShelf;

    /**
     * 厂商
     */
    private String manufacturer;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public List<GoodsChannel> getGoodsChannelParams() {
        return goodsChannelParams;
    }

    public void setGoodsChannelParams(List<GoodsChannel> goodsChannelParams) {
        this.goodsChannelParams = goodsChannelParams;
    }

    public String getGoodsShelf() {
        return goodsShelf;
    }

    public void setGoodsShelf(String goodsShelf) {
        this.goodsShelf = goodsShelf;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public static class GoodsChannel implements Serializable {

        private static final long serialVersionUID = 5485152192819142683L;

        /**
         * 货道
         */
        private String goodsChannel;

        /**
         * skuId
         */
        private Long skuId;

        /**
         * 序号
         */
        private Integer sequence;

        /**
         * 容量
         */
        private Integer capacity;

        public String getGoodsChannel() {
            return goodsChannel;
        }

        public void setGoodsChannel(String goodsChannel) {
            this.goodsChannel = goodsChannel;
        }

        public Long getSkuId() {
            return skuId;
        }

        public void setSkuId(Long skuId) {
            this.skuId = skuId;
        }

        public Integer getSequence() {
            return sequence;
        }

        public void setSequence(Integer sequence) {
            this.sequence = sequence;
        }

        public Integer getCapacity() {
            return capacity;
        }

        public void setCapacity(Integer capacity) {
            this.capacity = capacity;
        }
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("storeId", storeId);
        map.put("goodsChannelParams", goodsChannelParams);
        map.put("goodsShelf", goodsShelf);
        map.put("manufacturer", manufacturer);
        list.add(map);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<SyncGoodsChannelsResult> getClassName() {
        return SyncGoodsChannelsResult.class;
    }
}
