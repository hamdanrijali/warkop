package com.pajk.justice.openapi.biz.request.store;

import com.pajk.justice.openapi.biz.model.reuslt.store.UpdateStorePolygonsByIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStorePolygonsByIdRequest implements IRequest<UpdateStorePolygonsByIdResult> {


    String apiGroup = "shennong";
    String apiName = "updateStorePolygonsById";
    String apiId = "c86b25cd0be64a16fab6287b2d7be7b8";


    private Long sellerId;

    private String storeReferId;

    private String polygons;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getStoreReferId() {
        return storeReferId;
    }

    public void setStoreReferId(String storeReferId) {
        this.storeReferId = storeReferId;
    }

    public String getPolygons() {
        return polygons;
    }

    public void setPolygons(String polygons) {
        this.polygons = polygons;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(storeReferId);
        list.add(polygons);
        return list;
    }


    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdateStorePolygonsByIdResult> getClassName() {
        return UpdateStorePolygonsByIdResult.class;
    }
}
