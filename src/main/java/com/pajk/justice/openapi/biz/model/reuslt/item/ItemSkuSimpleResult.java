package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class ItemSkuSimpleResult extends BaseDO {

    private static final long serialVersionUID = 4396572056731620835L;
    private Long spuId;
    private String spuOuterId;
    private Long skuId;
    private String skuOuterId;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public String getSpuOuterId() {
        return spuOuterId;
    }

    public void setSpuOuterId(String spuOuterId) {
        this.spuOuterId = spuOuterId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getSkuOuterId() {
        return skuOuterId;
    }

    public void setSkuOuterId(String skuOuterId) {
        this.skuOuterId = skuOuterId;
    }
}
