package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by cuidongchao on 2018/4/30.
 *
 * @author cuidongchao
 */
public class UpdateOrderResult extends BaseResult {

    private static final long serialVersionUID = 8821229554317530088L;

    public UpdateOrderResult() {
    }

    public UpdateOrderResult(int code, String msg) {
        super(code, msg);
    }
}
