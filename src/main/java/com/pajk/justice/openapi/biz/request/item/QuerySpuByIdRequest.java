package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.QuerySpuByIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class QuerySpuByIdRequest implements IRequest<QuerySpuByIdResult> {


    String apiId = "dbd954fe92410223b8bfa6b3b2d3a262";
    String apiName = "querySpuById";
    String apiGroup = "shennong";

    private Long sellerId;
    private Long spuId;


    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(spuId);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<QuerySpuByIdResult> getClassName() {
        return QuerySpuByIdResult.class;
    }
}
