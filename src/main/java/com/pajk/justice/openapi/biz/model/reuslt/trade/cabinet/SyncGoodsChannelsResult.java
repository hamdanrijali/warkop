package com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class SyncGoodsChannelsResult extends BaseResult {

    private static final long serialVersionUID = -582513895299725353L;

    public SyncGoodsChannelsResult() {}

    public SyncGoodsChannelsResult(int code, String msg) {
        super(code, msg);
    }
}
