package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class ItemSearchSpuResult extends BaseDO {
    private static final long serialVersionUID = -1810737945166712078L;

    private Long spuId;

    private String title;

    private Integer status;

    private String pictureUrl;

    private Integer skuCount;

    private String brandName;

    private String	isSecKill;

    private String	isCspu;

    private String	createdTime;

    private String	modifiedTime;


    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Integer getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(Integer skuCount) {
        this.skuCount = skuCount;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getIsSecKill() {
        return isSecKill;
    }

    public void setIsSecKill(String isSecKill) {
        this.isSecKill = isSecKill;
    }

    public String getIsCspu() {
        return isCspu;
    }

    public void setIsCspu(String isCspu) {
        this.isCspu = isCspu;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
