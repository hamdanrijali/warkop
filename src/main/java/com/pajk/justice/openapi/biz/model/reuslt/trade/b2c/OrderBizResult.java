package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;


/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderBizResult extends BaseDO {
    private static final long serialVersionUID = 2176435324885623717L;
    /**
     * pajk订单号
     */
    private String bizOrderId;
    /**
     * pajk店铺ID(B2C业务请忽略)
     */
    private Long storeId;
    /**
     *  pajk商家ID
     */
    private Long sellerId;

    /**
     * 购买商品数量
     */
    private Long buyAmount;

    /**
     * 是否有处方药(1代表有处方药)
     */
    private Integer isPrescribed;

    /**
     * 渠道号
     */
    private String channel;

    /**
     * 支付时间
     */
    private Long payTime;

    /**
     * 用户实际支付现金
     */
    private Long actualTotalFee;

    /**
     * 支付方式
     */
    private Integer payMode;

    /**
     * 订单状态
     */
    private String storeOrderStatus;

    /**
     * "普通订单" : "1"
     * "未审核处方药订单" : "2"
     * "已审核通过处方药订单" : "3"
     * "已审核未通过处方药订单" : "4"
     */
    private String storeOrderType;

    /**
     * 买家id
     * */
    private Long buyerId;

    /**
     * 订单创建时间 单位为秒 时间戳
     */
    private Long createTime;

    /**
     * 最后修改时间 单位秒 UTC 时间戳
     */
    private Long modifyTime;

    /**
     * 下单平台（中文描述）
     */
    private String platform;

    /**
     * 下单渠道
     */
    private String subBizType;

    /**
     * 商品信息
     */
    private List<OrderItemResult> items;

    public String getBizOrderId() {
        return bizOrderId;
    }

    public void setBizOrderId(String bizOrderId) {
        this.bizOrderId = bizOrderId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(Long buyAmount) {
        this.buyAmount = buyAmount;
    }

    public Integer getIsPrescribed() {
        return isPrescribed;
    }

    public void setIsPrescribed(Integer isPrescribed) {
        this.isPrescribed = isPrescribed;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public Long getActualTotalFee() {
        return actualTotalFee;
    }

    public void setActualTotalFee(Long actualTotalFee) {
        this.actualTotalFee = actualTotalFee;
    }

    public Integer getPayMode() {
        return payMode;
    }

    public void setPayMode(Integer payMode) {
        this.payMode = payMode;
    }

    public String getStoreOrderStatus() {
        return storeOrderStatus;
    }

    public void setStoreOrderStatus(String storeOrderStatus) {
        this.storeOrderStatus = storeOrderStatus;
    }

    public String getStoreOrderType() {
        return storeOrderType;
    }

    public void setStoreOrderType(String storeOrderType) {
        this.storeOrderType = storeOrderType;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSubBizType() {
        return subBizType;
    }

    public void setSubBizType(String subBizType) {
        this.subBizType = subBizType;
    }

    public List<OrderItemResult> getItems() {
        return items;
    }

    public void setItems(List<OrderItemResult> items) {
        this.items = items;
    }
}
