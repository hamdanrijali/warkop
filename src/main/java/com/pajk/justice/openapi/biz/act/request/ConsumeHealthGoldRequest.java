package com.pajk.justice.openapi.biz.act.request;

import com.pajk.justice.openapi.biz.act.result.ConsumeHealthGoldResult;
import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author DENGCHUHUA127 on 2018/11/23.
 * @since v2.4.0
 */
public class ConsumeHealthGoldRequest  extends UserBaseRequest implements IRequest<ConsumeHealthGoldResult> {

    private static final long serialVersionUID = -6659944213714157198L;
    private String activityCode;
    private String prizeCode;
    private String tradeUuid;
    private Long healthGold;

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getPrizeCode() {
        return prizeCode;
    }

    public void setPrizeCode(String prizeCode) {
        this.prizeCode = prizeCode;
    }

    public String getTradeUuid() {
        return tradeUuid;
    }

    public void setTradeUuid(String tradeUuid) {
        this.tradeUuid = tradeUuid;
    }

    public Long getHealthGold() {
        return healthGold;
    }

    public void setHealthGold(Long healthGold) {
        this.healthGold = healthGold;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("activityCode",activityCode);
        map.put("prizeCode",prizeCode);
        map.put("tradeUuid",tradeUuid);
        map.put("healthGold",healthGold);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "3b79add0ee608a6c7226133347bb69e4";
    }

    @Override
    public String getApiName() {
        return "consumeHealthGold";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_RUMEX;
    }

    @Override
    public Class<ConsumeHealthGoldResult> getClassName() {
        return ConsumeHealthGoldResult.class;
    }
}
