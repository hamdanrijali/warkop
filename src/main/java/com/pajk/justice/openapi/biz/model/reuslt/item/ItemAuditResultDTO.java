package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ItemAuditResultDTO extends BaseDO {

    private static final long serialVersionUID = 366773088390082388L;
    /**
     * 所属商品spuId
     */
    private Long spuId;
    /**上下架状态  */
    private Integer status;
    /**审核状态  */
    private Integer auditStatus;
    /**下架原因  */
    private String deListCause;
     /**审核失败原因 */
    private String auditFailCause;
    /** 创建时间  */
    private String createdTime;
    /**  修改时间  */
    private String modifiedTime;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getDeListCause() {
        return deListCause;
    }

    public void setDeListCause(String deListCause) {
        this.deListCause = deListCause;
    }

    public String getAuditFailCause() {
        return auditFailCause;
    }

    public void setAuditFailCause(String auditFailCause) {
        this.auditFailCause = auditFailCause;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
