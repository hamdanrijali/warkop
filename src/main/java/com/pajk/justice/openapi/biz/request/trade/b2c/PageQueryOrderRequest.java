package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.PageQueryOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
@Deprecated
public class PageQueryOrderRequest implements IRequest<PageQueryOrderResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "pageQueryB2COrderForOpenApi";
    private static final String apiId = "e4e0475234a625f4f78ca7477296b396";

    private Long sellerId;
    private Long startTime;
    private Long endTime;
    private String orderStatus;
    private String orderType;
    private int pageNo;
    private int pageSize;

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    //
//    encoder.addParameter(20226070806L); //sellerId pajk的卖家id 类型为long
//    encoder.addParameter(1466567056L); //startTime 开始时间,utc时间戳,单位秒,最多只能往前查询90天, 不能晚于当前查询时间.(支付时间) 类型为long
//    encoder.addParameter(1467172146L);//endTime 结束时间,utc时间戳,单位秒,必须大于等于 @startTime, 最晚不能晚于当前查询时间.(支付时间)(设定查询时间间隔为7天) 类型为long
//    encoder.addParameter("PAID"); //orderStatus 订单状态: 已支付未发货订单:PAID/已经退款订单:REFUND 类型为String
//    // B2C订单状态码（PAID、DELIVERY、REFUNDED、SUCCESS） B2C订单状态码（PAID、DELIVERY、REFUNDED、SUCCESS）
//    encoder.addParameter("0"); // orderType 订单类型 (0,"全部订单”),(1,"普通订单“),(2,“未审核处方药订单”), (3,"已审核通过处方药订单“),(4,"已审核未通过处方药订单”) 类型为String
//    encoder.addParameter(1); //pageNo 起始页编号,从1开始 类型为Integer
//    encoder.addParameter(40); //pageSize 每页数量, 目前最大为100, 大于100或则小于1 取值40 类型为Integer
//


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        list.add(sellerId);
        list.add(startTime);
        list.add(endTime);
        list.add(orderStatus);
        list.add(orderType);
        list.add(pageNo);
        list.add(pageSize);
        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<PageQueryOrderResult> getClassName() {
        return PageQueryOrderResult.class;
    }
}
