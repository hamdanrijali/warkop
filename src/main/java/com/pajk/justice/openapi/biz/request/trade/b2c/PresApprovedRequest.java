package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.PresApprovedResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PresApprovedRequest implements IRequest<PresApprovedResult> {

    String apiGroup = "shennong";
    String apiName = "presApproved";
    String apiId = "3274417fc64093277d2243b69afd72e3";

    private Integer roleType;
    private Long sellerId;
    private String tradeId;

    public Integer getRoleType() {
        return roleType;
    }

    public void setRoleType(Integer roleType) {
        this.roleType = roleType;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("roleType",roleType);
        map.put("sellerId",sellerId);
        map.put("tradeId",tradeId);
        list.add(map);
        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<PresApprovedResult> getClassName() {
        return PresApprovedResult.class;
    }
}
