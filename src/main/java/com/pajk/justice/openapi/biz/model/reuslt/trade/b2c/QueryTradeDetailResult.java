package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class QueryTradeDetailResult  extends BaseResult {
    private static final long serialVersionUID = -5134817948893244314L;

    private TradeDetailResult model;


    public TradeDetailResult getModel() {
        return model;
    }

    public void setModel(TradeDetailResult model) {
        this.model = model;
    }


    public QueryTradeDetailResult(int code, String msg){
        super(code, msg);
    }

    public QueryTradeDetailResult(){};
}
