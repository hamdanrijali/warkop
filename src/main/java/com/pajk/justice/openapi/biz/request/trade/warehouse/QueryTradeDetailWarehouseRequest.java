package com.pajk.justice.openapi.biz.request.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse.QueryTradeDetailWarehouseResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class QueryTradeDetailWarehouseRequest implements IRequest<QueryTradeDetailWarehouseResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "queryTradeDetailWarehouse";
    private static final String apiId = "0b47b489002bf49f3387b3a78e85b852";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 订单id
     */
    private String tradeId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    @Override
    public List<?> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("warehouseId", warehouseId);
        map.put("tradeId", tradeId);

        List<Object> list = new LinkedList<>();
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<QueryTradeDetailWarehouseResult> getClassName() {
        return QueryTradeDetailWarehouseResult.class;
    }

}
