package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class QueryRefundOrderResult extends BaseResult {

    private static final long serialVersionUID = -3515701754540062794L;

    private RefundOrderResult model;

    public RefundOrderResult getModel() {
        return model;
    }

    public void setModel(RefundOrderResult model) {
        this.model = model;
    }


    public QueryRefundOrderResult() {

    }

    public QueryRefundOrderResult(int code, String msg) {
        super(code, msg);

    }
}
