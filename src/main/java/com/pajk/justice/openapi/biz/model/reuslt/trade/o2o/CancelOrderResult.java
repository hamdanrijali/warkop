package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;
/**
 * Created by yangguoqing on 2018/7/24.
 *
 * @author yangguoqing
 */
public class CancelOrderResult extends BaseResult {
    private static final long serialVersionUID = 3333867428930962649L;

    public CancelOrderResult(){

    }
    public CancelOrderResult(int code, String msg){

    }
}
