package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.VoidResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ListingItemApplyRequest extends BaseRequest implements IRequest<VoidResult> {

    private static final long serialVersionUID = 1030915444982880300L;
    private Long spuId;

    private List<Long> skuIds;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("spuId",spuId);
        map.put("skuIds",skuIds);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "c839208be10b4bd7f48230b5c0474f79";
    }

    @Override
    public String getApiName() {
        return "listingItemApply";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<VoidResult> getClassName() {
        return VoidResult.class;
    }

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public List<Long> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Long> skuIds) {
        this.skuIds = skuIds;
    }
}
