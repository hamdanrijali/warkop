package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class DelistSkuResult extends BaseResult {
    private static final long serialVersionUID = -6660967743264904096L;

    public DelistSkuResult() {
    }

    public DelistSkuResult(int code, String msg) {
        super(code, msg);
    }
}
