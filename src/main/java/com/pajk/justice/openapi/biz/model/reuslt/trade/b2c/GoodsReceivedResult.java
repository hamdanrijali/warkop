package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/27.
 * Modify by fanhuafeng on 18/4/27
 */
public class GoodsReceivedResult extends BaseResult {
    private static final long serialVersionUID = -1732577491906701853L;

    public GoodsReceivedResult() {
    }

    public GoodsReceivedResult(int code, String msg) {
        super(code, msg);
    }
}
