package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class AuthorizeCategoryResult extends BaseDO {
    private static final long serialVersionUID = -3523683208760019979L;


    private Long categoryId;
    private String categoryName;
    private String isCspu;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getIsCspu() {
        return isCspu;
    }

    public void setIsCspu(String isCspu) {
        this.isCspu = isCspu;
    }
}
