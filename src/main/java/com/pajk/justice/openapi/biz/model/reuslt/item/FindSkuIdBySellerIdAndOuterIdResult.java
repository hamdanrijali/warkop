package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class FindSkuIdBySellerIdAndOuterIdResult extends BaseResult {

    private static final long serialVersionUID = 494697652675647981L;

    private List<ItemSkuSimpleResult> model;

    public List<ItemSkuSimpleResult> getModel() {
        return model;
    }

    public void setModel(List<ItemSkuSimpleResult> model) {
        this.model = model;
    }

}
