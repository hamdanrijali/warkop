package com.pajk.justice.openapi.biz.model.reuslt.store;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStoreBusinessHoursResult extends BaseResult{
    private static final long serialVersionUID = -1726200477055121332L;

    public UpdateStoreBusinessHoursResult() {
    }

    public UpdateStoreBusinessHoursResult(int code, String msg) {
        super(code, msg);
    }
}
