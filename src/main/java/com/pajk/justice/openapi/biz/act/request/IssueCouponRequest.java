package com.pajk.justice.openapi.biz.act.request;

import com.pajk.justice.openapi.biz.act.result.IssueCouponResult;
import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author DENGCHUHUA127 on 2018/11/23.
 * @since v2.4.0
 */
public class IssueCouponRequest extends UserBaseRequest implements IRequest<IssueCouponResult> {

    private static final long serialVersionUID = -1568597191623501102L;
    private String activityCode;
    private String prizeCode;
    private String tradeUuid;


    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getPrizeCode() {
        return prizeCode;
    }

    public void setPrizeCode(String prizeCode) {
        this.prizeCode = prizeCode;
    }

    public String getTradeUuid() {
        return tradeUuid;
    }

    public void setTradeUuid(String tradeUuid) {
        this.tradeUuid = tradeUuid;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("activityCode",activityCode);
        map.put("prizeCode",prizeCode);
        map.put("tradeUuid",tradeUuid);

        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "f1b60b0a6cd4138d69edbd8e7c2c0594";
    }

    @Override
    public String getApiName() {
        return "issueCoupon";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_RUMEX;
    }

    @Override
    public Class<IssueCouponResult> getClassName() {
        return IssueCouponResult.class;
    }
}
