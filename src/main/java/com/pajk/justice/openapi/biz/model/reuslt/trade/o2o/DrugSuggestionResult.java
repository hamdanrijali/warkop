package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;

public class DrugSuggestionResult extends BaseDO {

    private static final long serialVersionUID = 7727540786442275301L;

    /**
     * 处方开具机构
     */
    private String hospitalCode;

    /**
     * 处方开具机构名称
     */
    private String hospitalName;

    /**
     * 开单科室
     */
    private String orderedBy;

    /**
     * 开方医生code
     */
    private String prescribedCode;

    /**
     * 开方医生
     */
    private String prescribedBy;

    /**
     * 处方开具日期
     */
    private String presComposed;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private String sex;

    /**
     * 患者年龄-eg:25岁
     */
    private String age;

    /**
     * 诊断结果
     */
    private String diagnosis;

    /**
     * 商品集合
     */
    private List<PrescriptionItemResult> items;

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(String orderedBy) {
        this.orderedBy = orderedBy;
    }

    public String getPrescribedCode() {
        return prescribedCode;
    }

    public void setPrescribedCode(String prescribedCode) {
        this.prescribedCode = prescribedCode;
    }

    public String getPrescribedBy() {
        return prescribedBy;
    }

    public void setPrescribedBy(String prescribedBy) {
        this.prescribedBy = prescribedBy;
    }

    public String getPresComposed() {
        return presComposed;
    }

    public void setPresComposed(String presComposed) {
        this.presComposed = presComposed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public List<PrescriptionItemResult> getItems() {
        return items;
    }

    public void setItems(List<PrescriptionItemResult> items) {
        this.items = items;
    }
}

