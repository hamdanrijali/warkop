package com.pajk.justice.openapi.biz.request.store;

import com.pajk.justice.openapi.biz.model.reuslt.store.UpdateStoreStatusResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStoreStatusRequest implements IRequest<UpdateStoreStatusResult> {

    String apiGroup = "shennong";
    String apiName = "updateStoreStatus";
    String apiId = "1576d7892ccb7833bd6f45690b031431";

    private Long sellerId;
    private String storeReferId;
    private String status;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getStoreReferId() {
        return storeReferId;
    }

    public void setStoreReferId(String storeReferId) {
        this.storeReferId = storeReferId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(storeReferId);
        list.add(status);
        return list;
    }


    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdateStoreStatusResult> getClassName() {
        return UpdateStoreStatusResult.class;
    }
}
