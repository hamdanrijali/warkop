package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.QueryRefundResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class QueryRefundRequest implements IRequest<QueryRefundResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "queryRefund";
    private static final String apiId = "104071317ebac50060fa711b8374d7a0";

    private Long sellerId;
    private String refundId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("refundId",refundId);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<QueryRefundResult> getClassName() {
        return QueryRefundResult.class;
    }

}
