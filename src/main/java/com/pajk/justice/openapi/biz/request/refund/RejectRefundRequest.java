package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.RejectRefundResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RejectRefundRequest implements IRequest<RejectRefundResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "rejectRefund";
    private static final String apiId = "ed31bc50c6896de1cfcbf1a652a4eb87";

    private Long sellerId;
    private String refundId;
    private String reason;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("refundId",refundId);
        map.put("reason",reason);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<RejectRefundResult> getClassName() {
        return RejectRefundResult.class;
    }

}
