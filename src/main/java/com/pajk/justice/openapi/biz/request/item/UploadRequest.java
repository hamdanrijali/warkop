package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.AddItemResult;
import com.pajk.justice.openapi.biz.model.reuslt.item.VoidResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class UploadRequest extends BaseRequest implements IRequest<VoidResult> {

    private static final long serialVersionUID = 8585153139612939132L;
    /** 外部关联id，spuOuterId或者skuOuterId*/
    private String outerId;

    /** value */
    private List<FileUploadParam> uploadFileList;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("outerId",outerId);
        map.put("uploadFileList",uploadFileList);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "b28598990b964320007e6ace94aca5a5";
    }

    @Override
    public String getApiName() {
        return "upload";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<VoidResult> getClassName() {
        return VoidResult.class;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId;
    }

    public List<FileUploadParam> getUploadFileList() {
        return uploadFileList;
    }

    public void setUploadFileList(List<FileUploadParam> uploadFileList) {
        this.uploadFileList = uploadFileList;
    }
}
