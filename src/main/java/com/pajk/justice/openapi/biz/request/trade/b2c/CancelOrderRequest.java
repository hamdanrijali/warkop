package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.CancelOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class CancelOrderRequest implements IRequest<CancelOrderResult> {

    String apiGroup = "shennong";
    String apiName = "cancelOrder";
    String apiId = "a6521c0ddd211bff699de0a36b507a31";




    private Long sellerId;
    private String tradeId;

    private String cancelMsg;


    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getCancelMsg() {
        return cancelMsg;
    }

    public void setCancelMsg(String cancelMsg) {
        this.cancelMsg = cancelMsg;
    }



    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(tradeId);
        list.add(cancelMsg);
        return list;
    }


    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<CancelOrderResult> getClassName() {
        return CancelOrderResult.class;
    }
}
