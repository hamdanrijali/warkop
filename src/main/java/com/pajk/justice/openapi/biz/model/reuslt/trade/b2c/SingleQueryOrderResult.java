package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class SingleQueryOrderResult extends BaseResult {


    private static final long serialVersionUID = -5134817948893244314L;

    private TradeFullResult model;


    public TradeFullResult getModel() {
        return model;
    }

    public void setModel(TradeFullResult model) {
        this.model = model;
    }


    public SingleQueryOrderResult(int code, String msg){
        super(code, msg);
    }

    public SingleQueryOrderResult(){};
}
