package com.pajk.justice.openapi.biz.act.request;

import com.pajk.justice.openapi.biz.act.result.IssueHealthGoldResult;
import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author DENGCHUHUA127 on 2018/11/23.
 * @since v2.4.0
 */
public class IssueHealthGoldRequest extends UserBaseRequest implements IRequest<IssueHealthGoldResult> {

    private static final long serialVersionUID = -1568597191623501102L;
    private String activityCode;
    private String prizeCode;
    private String tradeUuid;
    private Long healthGold;

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getPrizeCode() {
        return prizeCode;
    }

    public void setPrizeCode(String prizeCode) {
        this.prizeCode = prizeCode;
    }

    public String getTradeUuid() {
        return tradeUuid;
    }

    public void setTradeUuid(String tradeUuid) {
        this.tradeUuid = tradeUuid;
    }

    public Long getHealthGold() {
        return healthGold;
    }

    public void setHealthGold(Long healthGold) {
        this.healthGold = healthGold;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("activityCode",activityCode);
        map.put("prizeCode",prizeCode);
        map.put("tradeUuid",tradeUuid);
        map.put("healthGold",healthGold);

        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "02c7f7eb36e344a175b9fe4af46e8aa8";
    }

    @Override
    public String getApiName() {
        return "issueHealthGold";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_RUMEX;
    }

    @Override
    public Class<IssueHealthGoldResult> getClassName() {
        return IssueHealthGoldResult.class;
    }
}
