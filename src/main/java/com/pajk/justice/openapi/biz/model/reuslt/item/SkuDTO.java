package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class SkuDTO  extends BaseDO {

    private static final long serialVersionUID = -4237368875404273666L;

    /**  skuId   */
    private Long id;
    /**  所属商品spuId */
    private Long spuId;
    /**  商户商品编码    */
    private String skuOuterId;
    /**  销售单价(分)  */
    private Long price;
    /** 商品原价(分) */
    private Long origPrice;
    /**  sku主图地址   */
    private String pictures;
    /**  商品SKU上下架状态  */
    private Integer status;
    /**  按照规格level*key:level*spec:key:value拼装字符串  */
    private String specValues;

    /** 创建时间  */
    private String createdTime;
    /**  修改时间  */
    private String modifiedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public String getSkuOuterId() {
        return skuOuterId;
    }

    public void setSkuOuterId(String skuOuterId) {
        this.skuOuterId = skuOuterId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getOrigPrice() {
        return origPrice;
    }

    public void setOrigPrice(Long origPrice) {
        this.origPrice = origPrice;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSpecValues() {
        return specValues;
    }

    public void setSpecValues(String specValues) {
        this.specValues = specValues;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
