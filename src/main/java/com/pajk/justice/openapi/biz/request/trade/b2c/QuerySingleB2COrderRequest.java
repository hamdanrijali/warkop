package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.SingleQueryOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
@Deprecated
public class QuerySingleB2COrderRequest implements IRequest<SingleQueryOrderResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "querySingleB2COrderForOpenApi";
    private static final String apiId = "0a296dafe841090172ee80af2307087c";


    private Long sellerId;
    private String tradeId;


    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        list.add(sellerId);
        list.add(tradeId);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<SingleQueryOrderResult> getClassName() {
        return SingleQueryOrderResult.class;
    }

}
