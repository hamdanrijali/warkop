package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.QuerySelfDeliveryTradeResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class QuerySelfDeliveryTradeRequest implements IRequest<QuerySelfDeliveryTradeResult> {


    String apiGroup = "shennong";
    String apiName = "querySelfDeliveryTrade";
    String apiId = "797b28ff57c9e85a0ef86ee2173b3ea0";


    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private Long storeId;

    /**
     * 外部门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private String outStoreId;

    /**
     * 取货验证码（必填）
     */
    private String verifyCode;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }




    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("outStoreId", outStoreId);
        map.put("storeId", storeId);
        map.put("verifyCode", verifyCode);
        list.add(map);
        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<QuerySelfDeliveryTradeResult> getClassName() {
        return QuerySelfDeliveryTradeResult.class;
    }

}
