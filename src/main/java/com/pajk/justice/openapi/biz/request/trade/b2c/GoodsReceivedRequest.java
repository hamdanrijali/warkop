package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.GoodsReceivedResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/27.
 * Modify by fanhuafeng on 18/4/27
 */
public class GoodsReceivedRequest implements IRequest<GoodsReceivedResult> {

    String apiGroup = "shennong";
    String apiName = "goodsReceived";
    String apiId = "aefd06deced038af6332084b5d1fbb99";


    private Long sellerId;
    private String tradeId;


    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(tradeId);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<GoodsReceivedResult> getClassName() {
        return GoodsReceivedResult.class;
    }
}
