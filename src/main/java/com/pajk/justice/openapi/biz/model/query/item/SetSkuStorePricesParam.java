package com.pajk.justice.openapi.biz.model.query.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SetSkuStorePricesParam extends BaseDO {

    private static final long serialVersionUID = 9082994447998682627L;
    public Long skuId;

    private Long price;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
