package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.UpdatePriceResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

public class UpdatePriceRequest implements IRequest<UpdatePriceResult> {

    String apiId = "7cbb253647549c2b9613149dbce0c4a5";
    String apiName = "updatePrice";
    String apiGroup = "shennong";

    private Long sellerId;
    private Long skuId;
    private Long storeId;
    private String storeReferId;
    private String priceType;
    private Long price;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreReferId() {
        return storeReferId;
    }

    public void setStoreReferId(String storeReferId) {
        this.storeReferId = storeReferId;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }


    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(skuId);
        list.add(storeId);
        list.add(storeReferId);
        list.add(priceType);
        list.add(price);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdatePriceResult> getClassName() {
        return UpdatePriceResult.class;
    }
}
