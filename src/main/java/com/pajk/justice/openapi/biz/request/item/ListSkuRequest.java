package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.ListSkuResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class ListSkuRequest implements IRequest<ListSkuResult> {

    String apiGroup = "shennong";
    String apiName = "listSku";
    String apiId = "de594726855da2c3cda54afe560a5a01";


    private Long sellerId;
    private Long skuId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("skuId",skuId);
        list.add(map);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<ListSkuResult> getClassName() {
        return ListSkuResult.class;
    }
}
