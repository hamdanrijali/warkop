package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class CancelOrderResult extends BaseResult {
    private static final long serialVersionUID = -1318935337374712519L;

    public CancelOrderResult() {
    }

    public CancelOrderResult(int code, String msg) {
        super(code, msg);
    }
}
