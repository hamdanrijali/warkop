package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SetSkuStorePricesResult extends BaseResult {

    private static final long serialVersionUID = -3080680352543498682L;


    private List<String> model;

    public SetSkuStorePricesResult() {
    }

    public SetSkuStorePricesResult(int code, String msg) {
        super(code, msg);
    }

    public List<String> getModel() {
        return model;
    }

    public void setModel(List<String> model) {
        this.model = model;
    }
}
