package com.pajk.justice.openapi.biz.request.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse.TradeDeliveryWarehouseResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeDeliveryWarehouseRequest implements IRequest<TradeDeliveryWarehouseResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "tradeDeliveryWarehouse";
    private static final String apiId = "cfd527afcaa800ac7c343fda82916c18";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 订单id
     */
    private String tradeId;

    /**
     * 物流公司唯一识别 通过<查询物流公司列表接口>获取流程公司id
     */
    private Long carrierId;

    /**
     * 快递单号
     */
    private String trackingNumber;

    /**
     * 操作员
     */
    private String operator;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public List<?> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("warehouseId", warehouseId);
        map.put("tradeId", tradeId);
        map.put("carrierId", carrierId);
        map.put("trackingNumber", trackingNumber);
        map.put("operator", operator);

        List<Object> list = new LinkedList<>();
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<TradeDeliveryWarehouseResult> getClassName() {
        return TradeDeliveryWarehouseResult.class;
    }

}
