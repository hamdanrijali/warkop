package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class RefundPaymentResult extends BaseDO {

    private static final long serialVersionUID = 2594916639790674768L;

    /**
     * 平安商品skuId(邮费为空)
     */
    private String skuId;

    /**
     * 成交手段对谁支付
     */
    private String payFor;

    /**
     * 支付方式
     */
    private String method;

    /**
     * 退款金额(分)
     */
    private Long amount;

    /**
     * 平台承担比例, 目前只有当 method=COUPON 或者 PROMOTION 的时候, 该字段才会生效
     * 格式为 "0.0" ~ "1.0"
     */
    private String platformRatio;

    /**
     * 退款支付渠道, 只有method=CASH 时候有值
     */
    private String payChannel;

    /**
     * 退款平安内部订单号
     */
    private String settleNo;

    /**
     * 退款支付流水号, 只有method=CASH 时候有值
     */
    private String extTransFlow;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getPayFor() {
        return payFor;
    }

    public void setPayFor(String payFor) {
        this.payFor = payFor;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getPlatformRatio() {
        return platformRatio;
    }

    public void setPlatformRatio(String platformRatio) {
        this.platformRatio = platformRatio;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public String getSettleNo() {
        return settleNo;
    }

    public void setSettleNo(String settleNo) {
        this.settleNo = settleNo;
    }

    public String getExtTransFlow() {
        return extTransFlow;
    }

    public void setExtTransFlow(String extTransFlow) {
        this.extTransFlow = extTransFlow;
    }

}
