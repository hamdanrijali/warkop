package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class QuerySkuByIdResult extends BaseResult {
    private static final long serialVersionUID = -4392368430611582980L;

    private ItemSkuResult model;

    public ItemSkuResult getModel() {
        return model;
    }

    public void setModel(ItemSkuResult model) {
        this.model = model;
    }
}
