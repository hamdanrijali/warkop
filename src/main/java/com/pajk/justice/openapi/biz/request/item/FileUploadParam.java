package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author dengchuhua on 2018/7/24.
 * @since v2.3.0
 */
public class FileUploadParam extends BaseDO {
    private static final long serialVersionUID = 1904075279000161L;
    /** 图片的fileKey，新建的不传，更新图片的时候传*/
    private String key;
    /**1：spu图  2:商详图 3:sku图片 4:商详页 */
    private Integer bizType;
    /** 图片的序号*/
    private Integer position;

    /** 图片的绝对地址，要确定根据这个url可以下载到图片 */
    private String sourceURL;

    public FileUploadParam(){

    }

    public FileUploadParam(Integer bizType, Integer position, String sourceURL) {
        this.bizType = bizType;
        this.position = position;
        this.sourceURL = sourceURL;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getSourceURL() {
        return sourceURL;
    }

    public void setSourceURL(String sourceURL) {
        this.sourceURL = sourceURL;
    }

    public Integer getBizType() {
        return bizType;
    }

    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FileUploadParam that = (FileUploadParam) o;

        return new EqualsBuilder()
                .append(key, that.key)
                .append(bizType, that.bizType)
                .append(position, that.position)
                .append(sourceURL, that.sourceURL)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(key)
                .append(bizType)
                .append(position)
                .append(sourceURL)
                .toHashCode();
    }
}
