package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.PageQueryRefundByTradeIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class PageQueryRefundByTradeIdRequest implements IRequest<PageQueryRefundByTradeIdResult> {
    private static final String apiGroup = "shennong";
    private static final String apiName = "pageQueryRefundByTradeId";
    private static final String apiId = "48ec948ba840b00a5bab47ceaa37a078";

    private Long sellerId;
    private String tradeId;
    private Integer pageNo;
    private Integer pageSize;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("tradeId",tradeId);
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<PageQueryRefundByTradeIdResult> getClassName() {
        return PageQueryRefundByTradeIdResult.class;
    }

}
