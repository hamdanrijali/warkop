package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class CategoryPropValuesDTO extends BaseDO {

    private static final long serialVersionUID = -5233313420955596163L;
    /** 类目ID */
    private Long categoryId;
    /** 属性ID */
    private Long propertyId;
    /** 属性名 */
    private String propertyName;
    /** 属性类型:1表示选择型 2表示填写型 */
    private Integer propertyType;
    /** 排序因子 */
    private Integer sortFactor;
    /** 属性的状态:1表示有效 0表示无效 */
    private Integer status;
    /** 类目属性是否是单选项:1表示是单选 0表示是多选 */
    private Boolean isSingle;
    /** 类目属性是否需要在商详页展示，1表示需要，0表示不需要 */
    private Boolean isInDetails;
    /** 类目属性是否是搜索结果展示过滤项，1表示是，0表示不是 */
    private Boolean isFilter;
    /** 类目属性是否是必填项，1表示是，0表示不是 */
    private Boolean isRequired;
    /** （填写型属性）属性的值 */
    private String value;
    /** （选择型属性）属性值选项选择 */
    private List<PropOptionSelection> valueOptions;
    public CategoryPropValuesDTO(){

    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Integer getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(Integer propertyType) {
        this.propertyType = propertyType;
    }

    public Integer getSortFactor() {
        return sortFactor;
    }

    public void setSortFactor(Integer sortFactor) {
        this.sortFactor = sortFactor;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSingle() {
        return isSingle;
    }

    public void setSingle(Boolean single) {
        isSingle = single;
    }

    public Boolean getInDetails() {
        return isInDetails;
    }

    public void setInDetails(Boolean inDetails) {
        isInDetails = inDetails;
    }

    public Boolean getFilter() {
        return isFilter;
    }

    public void setFilter(Boolean filter) {
        isFilter = filter;
    }

    public Boolean getRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<PropOptionSelection> getValueOptions() {
        return valueOptions;
    }

    public void setValueOptions(List<PropOptionSelection> valueOptions) {
        this.valueOptions = valueOptions;
    }
}
