package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class AddItemResult extends BaseResult {


    private static final long serialVersionUID = -6108261573819945282L;

    private String model;


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
