package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.query.item.SetSkuStoreStocksParam;
import com.pajk.justice.openapi.biz.model.reuslt.item.SetSkuStoreStocksResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SetSkuStoreStocksRequest  implements IRequest<SetSkuStoreStocksResult> {

    String apiGroup = "shennong";
    String apiName = "setSkuStoreStocks";
    String apiId = "d5e2266a5491e613ec0ebcab4808f8dd";


    private Long sellerId;

    private Long storeId;

    private String storeOuterId;

    private List<SetSkuStoreStocksParam> skuStoreStockList;


    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreOuterId() {
        return storeOuterId;
    }

    public void setStoreOuterId(String storeOuterId) {
        this.storeOuterId = storeOuterId;
    }

    public List<SetSkuStoreStocksParam> getSkuStoreStockList() {
        return skuStoreStockList;
    }

    public void setSkuStoreStockList(List<SetSkuStoreStocksParam> skuStoreStockList) {
        this.skuStoreStockList = skuStoreStockList;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId );
        map.put("storeId", storeId);
        map.put("storeOuterId", storeOuterId);
        map.put("skuStoreStockList", skuStoreStockList);
        list.add(map);
        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<SetSkuStoreStocksResult> getClassName() {
        return SetSkuStoreStocksResult.class;
    }

}
