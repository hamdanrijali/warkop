package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.Date;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class StoreTradeResult extends BaseDO {

    private static final long serialVersionUID = -6295048823674556067L;
    /**
     * 订单号
     */
    private String tradeId;

    /**
     * 卖家ID
     */
    private Long sellerId;

    /**
     * 门店ID
     */
    private Long storeId;

    /**
     * 外部门店ID
     */
    private String outStoreId;

    /**
     * 取货验证码
     */
    private String verifyCode;

    /**
     * 订单商品信息
     */
    private List<StoreTradeItemResult> items;

    /**
     * 订单创建时间
     */
    private Date createTime;

    /**
     * 订单支付时间
     */
    private Date payTime;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public List<StoreTradeItemResult> getItems() {
        return items;
    }

    public void setItems(List<StoreTradeItemResult> items) {
        this.items = items;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}
