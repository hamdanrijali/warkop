package com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class PageQueryTradeWarehouseResult extends PageResult {

    private static final long serialVersionUID = 8982725979418013603L;

    private List<TradeWarehouseResult> model;

    public List<TradeWarehouseResult> getModel() {
        return model;
    }

    public void setModel(List<TradeWarehouseResult> model) {
        this.model = model;
    }

    public PageQueryTradeWarehouseResult() {
    }

    public PageQueryTradeWarehouseResult(int code, String msg) {
        super(code, msg);
    }

}
