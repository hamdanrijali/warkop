package com.pajk.justice.openapi.biz.model.reuslt.base;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class QueryResult extends BaseResult {

    private static final long serialVersionUID = -5113014260599963962L;
    private String model;


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public QueryResult(int code, String msg) {
        super(code, msg);

    }


    public QueryResult(){}
}
