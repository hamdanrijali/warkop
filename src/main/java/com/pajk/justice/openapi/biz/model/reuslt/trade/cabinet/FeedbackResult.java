package com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/28.
 * Modify by fanhuafeng on 18/4/28
 */
public class FeedbackResult extends BaseResult {
    private static final long serialVersionUID = -5912259322091561476L;

    public FeedbackResult() {
    }

    public FeedbackResult(int code, String msg) {
        super(code, msg);
    }
}
