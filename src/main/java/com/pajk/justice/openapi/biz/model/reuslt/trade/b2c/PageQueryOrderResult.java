package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class PageQueryOrderResult extends PageResult{

    private static final long serialVersionUID = 5003938362908641108L;
    private List<TradeFullResult> model;


    public List<TradeFullResult> getModel() {
        return model;
    }

    public void setModel(List<TradeFullResult> model) {
        this.model = model;
    }

    public PageQueryOrderResult() {
    }

    public PageQueryOrderResult(int code, String msg) {
        super(code, msg);
    }
}
