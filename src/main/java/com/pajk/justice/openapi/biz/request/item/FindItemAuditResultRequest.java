package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindItemAuditResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindItemAuditResultRequest extends BaseRequest implements IRequest<FindItemAuditResult> {

    private static final long serialVersionUID = 88636915149315017L;
    private Long id;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "ec890ac4cfb68b1862891169b62e7e9c";
    }

    @Override
    public String getApiName() {
        return "findItemAuditResult";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<FindItemAuditResult> getClassName() {
        return FindItemAuditResult.class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
