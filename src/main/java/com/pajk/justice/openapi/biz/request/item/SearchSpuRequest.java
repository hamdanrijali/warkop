package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.SearchSpuResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SearchSpuRequest implements IRequest<SearchSpuResult> {


    String apiId = "5f68bbbc62bbd22b2bae6e307e056746";
    String apiName = "searchSpu";
    String apiGroup = "shennong";


    private Long sellerId;
    private String title;
    private Long categoryId;
    private Integer status;
    private Integer pageNo;
    private Integer pageSize;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(title);
        list.add(categoryId);
        list.add(status);
        list.add(pageNo);
        list.add(pageSize);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<SearchSpuResult> getClassName() {
        return SearchSpuResult.class;
    }
}
