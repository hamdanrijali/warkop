package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderItemResult extends BaseDO{

    private static final long serialVersionUID = -4980846687636812015L;
    /**
     *pajk商品ID
     */
    private Long itemId;
    /**
     *pajk商品skuID
     */
    private Long itemSkuId;
    /**
     * 购买数量
     */
    private Long buyAmount;

    /**
     * 合作方商品ID
     */
    private String referId;

    /**
     * 是否处方药(1代表有处方药, 0代表没有处方药)
     */
    private Integer isPrescribed = 0;
    /**
     * 商品名
     */
    private String itemName;
    /**
     * 实际价格,销售价格,单位分
     */
    private Long actualPrice;

    /**
     * 结算价格(默认为0,代表没有结算价格)
     */
    private Long settlementPrice;

    /**
     * 返点比例(0~100, 小数点后最多1位)
     */
    private String rebateRatio;

    /**
     * 退款商品数量
     */
    private Integer refundCnt ;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(Long itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public Long getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(Long buyAmount) {
        this.buyAmount = buyAmount;
    }

    public String getReferId() {
        return referId;
    }

    public void setReferId(String referId) {
        this.referId = referId;
    }

    public Integer getIsPrescribed() {
        return isPrescribed;
    }

    public void setIsPrescribed(Integer isPrescribed) {
        this.isPrescribed = isPrescribed;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Long actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Long getSettlementPrice() {
        return settlementPrice;
    }

    public void setSettlementPrice(Long settlementPrice) {
        this.settlementPrice = settlementPrice;
    }

    public String getRebateRatio() {
        return rebateRatio;
    }

    public void setRebateRatio(String rebateRatio) {
        this.rebateRatio = rebateRatio;
    }

    public Integer getRefundCnt() {
        return refundCnt;
    }

    public void setRefundCnt(Integer refundCnt) {
        this.refundCnt = refundCnt;
    }
}
