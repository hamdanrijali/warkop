package com.pajk.justice.openapi.biz.common;


import org.apache.commons.lang3.StringUtils;

/**
 * Created by fanhuafeng on 18/4/17.
 * Modify by fanhuafeng on 18/4/17
 */
public enum QueryEnv {
    DEV("LATEST", "开发环境", "http://openapi.dev.pajk.cn/api/v1/", true),
    TEST("TEST", "测试环境", "http://openapi.test.pajk.cn/api/v1/", true),
    PROD("PROD", "生产环境", "http://openapi.en.jk.cn/api/v1/", false);

    private String url;
    private String code;
    private String msg;
    private boolean printFlag;

    QueryEnv(String code, String msg, String url, boolean printFlag) {
        this.code = code;
        this.msg = msg;
        this.url = url;
        this.printFlag = printFlag;
    }

    public String getUrl() {
        return url;
    }

    public String getCode() {
        return code;
    }

    public boolean getPrintFlag() {
        return printFlag;
    }


    public boolean isEquals(QueryEnv env) {
        return env != null && StringUtils.equals(this.code, env.getCode());
    }

    public boolean equanls(QueryEnv env) {
        return env != null && StringUtils.equals(this.code, env.getCode());
    }
}