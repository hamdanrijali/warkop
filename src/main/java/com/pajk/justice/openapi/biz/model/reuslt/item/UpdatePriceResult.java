package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class UpdatePriceResult extends BaseResult {
    private static final long serialVersionUID = 8882292215251809784L;

    public UpdatePriceResult() {
    }

    public UpdatePriceResult(int code, String msg) {
        super(code, msg);
    }
}
