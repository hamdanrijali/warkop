package com.pajk.justice.openapi.biz.model.reuslt.base;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class PageResult extends BaseResult {


    private int totalCount;

    private int pageNo;

    private int pageSize;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public PageResult() {
    }

    public PageResult(int code, String msg) {
        super(code, msg);
    }
}
