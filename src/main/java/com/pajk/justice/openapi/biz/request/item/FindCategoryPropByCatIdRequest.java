package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindCategoryPropByCatIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindCategoryPropByCatIdRequest extends BaseRequest implements IRequest<FindCategoryPropByCatIdResult> {

    private static final long serialVersionUID = 4519191562968567081L;
    private Long id;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "3d566b2b8c772cfeb828f38d3d6c5a47";
    }

    @Override
    public String getApiName() {
        return "findCategoryPropByCatId";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<FindCategoryPropByCatIdResult> getClassName() {
        return FindCategoryPropByCatIdResult.class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
