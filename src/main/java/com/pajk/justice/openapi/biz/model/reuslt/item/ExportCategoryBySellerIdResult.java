package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ExportCategoryBySellerIdResult extends BaseResult {
    private static final long serialVersionUID = 7439605227833178709L;
    private List<CategoryDTO> model;

    public List<CategoryDTO> getModel() {
        return model;
    }

    public void setModel(List<CategoryDTO> model) {
        this.model = model;
    }
}
