package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.UpdateOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cuidongchao on 2018/4/30.
 *
 * @author cuidongchao
 */
public class UpdateOrderRequest implements IRequest<UpdateOrderResult> {

    private static final String apiId = "a5ee105dd567cda4662d691c65b6f580";
    private static final String apiGroup = "shennong";
    private static final String apiName = "updateOrderByStorePartner";

    /**
     * PAJK卖家id
     */
    private Long sellerId;

    /**
     * 外部商户门店id
     */
    private String outStoreId;

    /**
     * 订单号
     */
    private String tradeId;

    /**
     * 状态(1:药店接单/骑手待接单, 2:骑手接单, 3:发货, 4:完成配送, 6:取消订单(自发货物流))
     */
    private Integer status;

    /**
     * 验证码(主要用于 药店发货以及用户收货),有些不需要验证码的默认填写"000000"
     */
    private String verifyCode = "000000";

    /**
     * 骑手昵称
     */
    private String riderName = "0";

    /**
     * 骑手电话
     */
    private String rideTel = "0";

    /**
     * 退款理由
     */
    private String cancelReason = "0";

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getRideTel() {
        return rideTel;
    }

    public void setRideTel(String rideTel) {
        this.rideTel = rideTel;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(outStoreId);
        list.add(tradeId);
        list.add(status);
        list.add(verifyCode);
        list.add(riderName);
        list.add(rideTel);
        list.add(cancelReason);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdateOrderResult> getClassName() {
        return UpdateOrderResult.class;
    }
}
