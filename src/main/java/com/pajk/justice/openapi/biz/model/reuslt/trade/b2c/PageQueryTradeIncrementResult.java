package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class PageQueryTradeIncrementResult extends PageResult {
    private static final long serialVersionUID = 5003938362908641108L;
    private List<TradeResult> model;


    public List<TradeResult> getModel() {
        return model;
    }

    public void setModel(List<TradeResult> model) {
        this.model = model;
    }

    public PageQueryTradeIncrementResult() {
    }

    public PageQueryTradeIncrementResult(int code, String msg) {
        super(code, msg);
    }
}
