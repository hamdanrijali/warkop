package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class PrescriptionItemResult extends BaseDO {

    private static final long serialVersionUID = -1975161022498373701L;

    /**
     * 平安skuId
     */
    private String skuId;

    /**
     * 一次用量
     */
    private String dosage;

    /**
     * 频率
     */
    private String frequency;

    /**
     * 用量单位
     */
    private String doseUnit;

    /**
     * 数量
     */
    private String totals;

    /**
     * 用法
     */
    private String drugUsage;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getTotals() {
        return totals;
    }

    public void setTotals(String totals) {
        this.totals = totals;
    }

    public String getDrugUsage() {
        return drugUsage;
    }

    public void setDrugUsage(String drugUsage) {
        this.drugUsage = drugUsage;
    }
}

