package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RefundCouponResult extends BaseDO {
    private static final long serialVersionUID = 1904075534872220101L;
    /**
     * 支付方式
     */
    private String method;
    /**
     * 对谁支付
     */
    private String payFor;
    /**
     * 商品id
     */
    private String skuId;
    /**
     * 支付方式
     */
    private String discountFee;
    /**
     * 平台承担比例
     */
    private String platformRatio;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPayFor() {
        return payFor;
    }

    public void setPayFor(String payFor) {
        this.payFor = payFor;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(String discountFee) {
        this.discountFee = discountFee;
    }

    public String getPlatformRatio() {
        return platformRatio;
    }

    public void setPlatformRatio(String platformRatio) {
        this.platformRatio = platformRatio;
    }
}
