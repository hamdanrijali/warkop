package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.BatchQuerySellerCategoryResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class BatchQuerySellerCategoryRequest implements IRequest<BatchQuerySellerCategoryResult> {

    String apiId = "10f1e1abcbf276da37223d5b54bbee7b";
    String apiName = "batchQuerySellerCategory";
    String apiGroup = "shennong";


    private Long sellerId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        list.add(sellerId);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }




    @Override
    public Class<BatchQuerySellerCategoryResult> getClassName() {
        return BatchQuerySellerCategoryResult.class;
    }
}
