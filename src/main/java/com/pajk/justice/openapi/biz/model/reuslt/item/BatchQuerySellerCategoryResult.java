package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class BatchQuerySellerCategoryResult extends BaseResult {
    private static final long serialVersionUID = -8835410714757385469L;

    private List<AuthorizeCategoryResult> model;

    public List<AuthorizeCategoryResult> getModel() {
        return model;
    }

    public void setModel(List<AuthorizeCategoryResult> model) {
        this.model = model;
    }
}
