package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.VoidResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class UpdateSkuRequest extends BaseRequest implements IRequest<VoidResult> {

    private static final long serialVersionUID = 6886009832563098219L;
    /** skuId*/
    private Long id;

    /** 售价/现价/基础价，单位是“分”*/
    private Long price;
    /**原价 ，单位是“分”*/
    private Long origPrice;

    /**
     * 该属性只有流量商品才能设置。
     */
    private String levelsValue;

    /**  levels要和spu上保持一致*/
    private String level1Spec;

    private String level2Spec;

    private String level3Spec;

    private String level4Spec;

    private String level5Spec;

    private String skuOuterId;

    private String pictures;
    /**重量（千克），支持3位小数 */
    private String weight;

    public Boolean supportShopCard;
    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        map.put("price",price);
        map.put("origPrice",origPrice);
        map.put("levelsValue",levelsValue);
        map.put("level1Spec",level1Spec);
        map.put("level2Spec",level2Spec);
        map.put("level3Spec",level3Spec);
        map.put("level4Spec",level4Spec);
        map.put("level5Spec",level5Spec);
        map.put("skuOuterId",skuOuterId);
        map.put("pictures",pictures);
        map.put("weight",weight);
        map.put("pictures",pictures);
        map.put("supportShopCard",supportShopCard);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "7d5820328410c557045842c1ba63a9db";
    }

    @Override
    public String getApiName() {
        return "updateSku";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<VoidResult> getClassName() {
        return VoidResult.class;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getOrigPrice() {
        return origPrice;
    }

    public void setOrigPrice(Long origPrice) {
        this.origPrice = origPrice;
    }

    public String getLevelsValue() {
        return levelsValue;
    }

    public void setLevelsValue(String levelsValue) {
        this.levelsValue = levelsValue;
    }

    public String getLevel1Spec() {
        return level1Spec;
    }

    public void setLevel1Spec(String level1Spec) {
        this.level1Spec = level1Spec;
    }

    public String getLevel2Spec() {
        return level2Spec;
    }

    public void setLevel2Spec(String level2Spec) {
        this.level2Spec = level2Spec;
    }

    public String getLevel3Spec() {
        return level3Spec;
    }

    public void setLevel3Spec(String level3Spec) {
        this.level3Spec = level3Spec;
    }

    public String getLevel4Spec() {
        return level4Spec;
    }

    public void setLevel4Spec(String level4Spec) {
        this.level4Spec = level4Spec;
    }

    public String getLevel5Spec() {
        return level5Spec;
    }

    public void setLevel5Spec(String level5Spec) {
        this.level5Spec = level5Spec;
    }

    public String getSkuOuterId() {
        return skuOuterId;
    }

    public void setSkuOuterId(String skuOuterId) {
        this.skuOuterId = skuOuterId;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Boolean getSupportShopCard() {
        return supportShopCard;
    }

    public void setSupportShopCard(Boolean supportShopCard) {
        this.supportShopCard = supportShopCard;
    }
}
