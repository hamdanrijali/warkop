package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.FindSkuStoreStocksResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class FindSkuStoreStocksRequest implements IRequest<FindSkuStoreStocksResult> {



    String apiGroup = "shennong";
    String apiName = "findSkuStoreStocks";
    String apiId = "c193d5ffcb9f15f8ee88ae9ccc667b30";

    private Long sellerId;
    private Long storeId;
    private String storeOuterId;
    private List<Long> skuIdList;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreOuterId() {
        return storeOuterId;
    }

    public void setStoreOuterId(String storeOuterId) {
        this.storeOuterId = storeOuterId;
    }

    public List<Long> getSkuIdList() {
        return skuIdList;
    }

    public void setSkuIdList(List<Long> skuIdList) {
        this.skuIdList = skuIdList;
    }


    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("storeId",storeId);
        map.put("storeOuterId",storeOuterId);
        map.put("skuIdList",skuIdList);

        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }




    @Override
    public Class<FindSkuStoreStocksResult> getClassName() {
        return FindSkuStoreStocksResult.class;
    }
}
