package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderPayItemResult extends BaseDO {
    private static final long serialVersionUID = -1219692528721514354L;

    /**
     * pajk所属使用的商品sku,可以为空
     */
    private String itemSkuId;

    /**
     * 支付方式
     * 参考 @PayToolDTO PAY_METHOD_*
     */
    private Integer method;

    /**
     * 支付金额(如果为现金,单位为分)
     */
    private Long amount;

    /**
     * 抵扣金额
     */
    private Long deductFee;


    /**
     * 促销活动 或则 优惠劵信息
     */
    private String promotionRefCode;

    /**
     * PAJK内部订单号
     */
    private String settleNo;

    /**
     * 第三方交易流水号
     */
    private String extTransFlow;

    /**
     * 第三方流水支付渠道
     */
    private String payChannel;

    /**
     * 第三方交易支付时间 (单位：秒)
     */
    private Long extTransTime;

    /**
     * 平台承担比例
     */
    private String platformRatio;

    /**
     * 成交手段对谁支付
     */
    private Integer payFor;

    public String getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(String itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getDeductFee() {
        return deductFee;
    }

    public void setDeductFee(Long deductFee) {
        this.deductFee = deductFee;
    }

    public String getPromotionRefCode() {
        return promotionRefCode;
    }

    public void setPromotionRefCode(String promotionRefCode) {
        this.promotionRefCode = promotionRefCode;
    }

    public String getSettleNo() {
        return settleNo;
    }

    public void setSettleNo(String settleNo) {
        this.settleNo = settleNo;
    }

    public String getExtTransFlow() {
        return extTransFlow;
    }

    public void setExtTransFlow(String extTransFlow) {
        this.extTransFlow = extTransFlow;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public Long getExtTransTime() {
        return extTransTime;
    }

    public void setExtTransTime(Long extTransTime) {
        this.extTransTime = extTransTime;
    }

    public String getPlatformRatio() {
        return platformRatio;
    }

    public void setPlatformRatio(String platformRatio) {
        this.platformRatio = platformRatio;
    }

    public Integer getPayFor() {
        return payFor;
    }

    public void setPayFor(Integer payFor) {
        this.payFor = payFor;
    }
}
