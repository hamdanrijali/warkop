package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class VoidResult extends BaseResult{

    private static final long serialVersionUID = 4055817528504077749L;

    public VoidResult() {
    }

    private VoidDTO model;

    public VoidResult(int code, String msg) {
        super(code, msg);
    }

    public VoidDTO getModel() {
        return model;
    }

    public void setModel(VoidDTO model) {
        this.model = model;
    }
}
