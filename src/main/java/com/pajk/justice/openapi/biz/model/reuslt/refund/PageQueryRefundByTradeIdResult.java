package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class PageQueryRefundByTradeIdResult extends PageResult {
    private static final long serialVersionUID = 5203938362908641108L;
    private List<RefundResult> model;


    public List<RefundResult> getModel() {
        return model;
    }

    public void setModel(List<RefundResult> model) {
        this.model = model;
    }

    public PageQueryRefundByTradeIdResult() {
    }

    public PageQueryRefundByTradeIdResult(int code, String msg) {
        super(code, msg);
    }
}

