package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ItemDTO extends BaseDO {

    private static final long serialVersionUID = 1028660676671950L;
    /** spuID  */
    private Long id;
    /**  商户ID */
    private Long sellerId;

    /** spuOuterId 选填*/
    private String spuOuterId;
    /** 商品类型 */
    private Integer type;
    /** 商品子类型 */
    private Integer subType;
    /** 批准文号 */
    private String approvalNo;
    /** 最大购买量 */
    private Integer orderQty;
    /**  三级类目ID */
    private Long categoryId;

    /** 商品标题 */
    private String title;
    /** 上下架状态 */
    private Integer status;
    /** 审核状态 */
    private Integer auditStatus;

    /** 厂商ID */
    private Long firmId;
    /** 厂商名->custom3 */
    private String firmName;
    /** 品牌ID */
    private Long brandId;
    /** 品牌名称 */
    private String brandName;

    /** 是否商城可见->custom1 */
    private Boolean isMallVisible;

    /** 券模版id->custom8 */
    private String voucherTemplateId;
    /** 详情 */
    private String detail;

    /** 规格 */
    private String specOnTitle;

    /** 条形码 */
    private String articleNo;
    /** 商品大图 */
    private String pictures;
    /** SKU维度数，最多5 */
    private Integer levels;
    /** SKU维度名,最多5个维度 */
    private String levelKeys;
    /**  标品id  */
    private Long cspuId;
    /**  创建时间  */
    private String createdTime;
    /** 修改时间   */
    private String modifiedTime;
    /** 邮费模版id*/
    private String postageTemplateId;

//扩展属性 spec
    /** 标题前文案*/
    private String prefixTitle;
    /** 副标题*/
    private String subHeading;
    /** 购买规则*/
    private String shoppingGuide;
    /** 物流提示*/
    private String logisticsTips;


    public ItemDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSpuOuterId() {
        return spuOuterId;
    }

    public void setSpuOuterId(String spuOuterId) {
        this.spuOuterId = spuOuterId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public String getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    public Integer getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Boolean getMallVisible() {
        return isMallVisible;
    }

    public void setMallVisible(Boolean mallVisible) {
        isMallVisible = mallVisible;
    }

    public String getVoucherTemplateId() {
        return voucherTemplateId;
    }

    public void setVoucherTemplateId(String voucherTemplateId) {
        this.voucherTemplateId = voucherTemplateId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSpecOnTitle() {
        return specOnTitle;
    }

    public void setSpecOnTitle(String specOnTitle) {
        this.specOnTitle = specOnTitle;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public Integer getLevels() {
        return levels;
    }

    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public String getLevelKeys() {
        return levelKeys;
    }

    public void setLevelKeys(String levelKeys) {
        this.levelKeys = levelKeys;
    }

    public String getPrefixTitle() {
        return prefixTitle;
    }

    public void setPrefixTitle(String prefixTitle) {
        this.prefixTitle = prefixTitle;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public String getShoppingGuide() {
        return shoppingGuide;
    }

    public void setShoppingGuide(String shoppingGuide) {
        this.shoppingGuide = shoppingGuide;
    }

    public String getLogisticsTips() {
        return logisticsTips;
    }

    public void setLogisticsTips(String logisticsTips) {
        this.logisticsTips = logisticsTips;
    }

    public String getPostageTemplateId() {
        return postageTemplateId;
    }

    public void setPostageTemplateId(String postageTemplateId) {
        this.postageTemplateId = postageTemplateId;
    }

    public Long getCspuId() {
        return cspuId;
    }

    public void setCspuId(Long cspuId) {
        this.cspuId = cspuId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
