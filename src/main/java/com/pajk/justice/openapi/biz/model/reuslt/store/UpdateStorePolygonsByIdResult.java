package com.pajk.justice.openapi.biz.model.reuslt.store;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStorePolygonsByIdResult extends BaseResult {
    private static final long serialVersionUID = 865994429278055547L;

    public UpdateStorePolygonsByIdResult() {
    }

    public UpdateStorePolygonsByIdResult(int code, String msg) {
        super(code, msg);
    }
}
