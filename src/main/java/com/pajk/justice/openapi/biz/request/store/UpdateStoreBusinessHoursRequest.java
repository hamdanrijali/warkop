package com.pajk.justice.openapi.biz.request.store;


import com.pajk.justice.openapi.biz.model.reuslt.store.UpdateStoreBusinessHoursResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStoreBusinessHoursRequest implements IRequest<UpdateStoreBusinessHoursResult>{

    String apiGroup = "shennong";
    String apiName = "updateStoreBusinessHours";
    String apiId = "d435183c7d7f01f03e9bd74875883e7a";


    private Long sellerId;
    private String storeReferId;
    private String openTime;
    private String closeTime;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getStoreReferId() {
        return storeReferId;
    }

    public void setStoreReferId(String storeReferId) {
        this.storeReferId = storeReferId;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(storeReferId);
        list.add(openTime);
        list.add(closeTime);
        return list;
    }


    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdateStoreBusinessHoursResult> getClassName() {
        return UpdateStoreBusinessHoursResult.class;
    }
}