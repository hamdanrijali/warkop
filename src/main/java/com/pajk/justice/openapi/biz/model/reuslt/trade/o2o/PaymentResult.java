package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class PaymentResult extends BaseDO {
    
    private static final long serialVersionUID = -4064484859357881691L;

    /**
     * 平安商品skuid(邮费为空)
     */
    private String skuId;

    /**
     * 支付方式
     */
    private String method;

    /**
     * 当method=CASH 或者 COD,代表支付金额，单位为分
     * 当method=PROMOTION, 代表抵扣次数
     */
    private Long amount;

    /**
     * 抵扣金额
     */
    private Long deductFee;

    /**
     * 当method=PROMOTION，促销活动
     * 当method=COUPON，优惠劵信息
     */
    private String promRefCode;

    /**
     * 支付渠道, 只有method=CASH 时候有值
     */
    private String payChannel;

    /**
     * 成交手段对谁支付
     */
    private String payFor;

    /**
     * 平台承担比例, 目前只有当method=COUPON 或者 PROMOTION 的时候, 该字段才会生效,
     * 格式为 "0.0" ~ "1.0"
     */
    private String platformRatio;

    /**
     * 平安内部订单号
     */
    private String settleNo;

    /**
     * 第三方交易流水号, 只有method=CASH 时候有值
     */
    private String extTransFlow;

    /**
     * 第三方支付平台支付时间，格式 yyyy-MM-dd HH:mm:ss, 只有method=CASH 时候有值
     */
    private String extTradeTime;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getDeductFee() {
        return deductFee;
    }

    public void setDeductFee(Long deductFee) {
        this.deductFee = deductFee;
    }

    public String getPromRefCode() {
        return promRefCode;
    }

    public void setPromRefCode(String promRefCode) {
        this.promRefCode = promRefCode;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public String getPayFor() {
        return payFor;
    }

    public void setPayFor(String payFor) {
        this.payFor = payFor;
    }

    public String getPlatformRatio() {
        return platformRatio;
    }

    public void setPlatformRatio(String platformRatio) {
        this.platformRatio = platformRatio;
    }

    public String getSettleNo() {
        return settleNo;
    }

    public void setSettleNo(String settleNo) {
        this.settleNo = settleNo;
    }

    public String getExtTransFlow() {
        return extTransFlow;
    }

    public void setExtTransFlow(String extTransFlow) {
        this.extTransFlow = extTransFlow;
    }

    public String getExtTradeTime() {
        return extTradeTime;
    }

    public void setExtTradeTime(String extTradeTime) {
        this.extTradeTime = extTradeTime;
    }


}
