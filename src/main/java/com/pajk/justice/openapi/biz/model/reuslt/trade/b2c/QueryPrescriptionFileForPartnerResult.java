package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class QueryPrescriptionFileForPartnerResult extends BaseResult {
    private static final long serialVersionUID = -5952259322091561476L;

    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public QueryPrescriptionFileForPartnerResult() {
    }

    public QueryPrescriptionFileForPartnerResult(int code, String msg) {
        super(code, msg);
    }
}
