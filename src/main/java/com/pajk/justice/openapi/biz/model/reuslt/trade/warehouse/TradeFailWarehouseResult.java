package com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeFailWarehouseResult extends BaseResult {

    private static final long serialVersionUID = 5633925375425095390L;

    public TradeFailWarehouseResult() {
    }

    public TradeFailWarehouseResult(Integer code, String msg) {
        super(code, msg);
    }

}
