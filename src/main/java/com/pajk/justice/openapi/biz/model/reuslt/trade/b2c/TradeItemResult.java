package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class TradeItemResult extends BaseDO {
    private static final long serialVersionUID = 1904075534882000101L;
    /**
     * 商品id
     */
    private String spuId;
    /**
     * skuId
     */
    private String skuId;
    /**
     * 外部商户skuId
     */
    private String outSkuId;
    /**
     * 商品名
     */
    private String title;
    /**
     * 商品售价
     */
    private String sellPrice;
    /**
     * 商品结算价
     */
    private String settlePrice;
    /**
     * 订单商品 实际支付现金
     */
    private String cashFee;
    /**
     * 订单商品税费 实际支付现金
     */
    private String cashTaxFee;
    /**
     * 商品佣金比率
     */
    private String rebateRatio;
    /**
     * 购买数量
     */
    private String buyCount;
    /**
     * 退款数量
     */
    private String refundCount;

    /**
     * 扩展属性
     * 如好眼睛验光属性
     */
    private String extProps;

    /**
     * 商品HS编码
     */
    private String hsCode;

    /**
     * 商品规格型号
     */
    private String itemSpec;

    /**
     * 产销国
     */
    private String originCountry;

    /**
     * 计量单位
     */
    private String goodsUnit;

    /**
     * 第一计量单位
     */
    private String firstUnit;

    /**
     * 第一计量单位数量
     */
    private String firstCount;

    /**
     * 第二计量单位
     */
    private String secondUnit;

    /**
     * 第二计量单位数量
     */
    private String secondCount;


    public String getSpuId() {
        return spuId;
    }

    public void setSpuId(String spuId) {
        this.spuId = spuId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getSettlePrice() {
        return settlePrice;
    }

    public void setSettlePrice(String settlePrice) {
        this.settlePrice = settlePrice;
    }

    public String getCashFee() {
        return cashFee;
    }

    public void setCashFee(String cashFee) {
        this.cashFee = cashFee;
    }

    public String getCashTaxFee() {
        return cashTaxFee;
    }

    public void setCashTaxFee(String cashTaxFee) {
        this.cashTaxFee = cashTaxFee;
    }

    public String getRebateRatio() {
        return rebateRatio;
    }

    public void setRebateRatio(String rebateRatio) {
        this.rebateRatio = rebateRatio;
    }

    public String getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(String buyCount) {
        this.buyCount = buyCount;
    }

    public String getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(String refundCount) {
        this.refundCount = refundCount;
    }

    public String getExtProps() {
        return extProps;
    }

    public void setExtProps(String extProps) {
        this.extProps = extProps;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getItemSpec() {
        return itemSpec;
    }

    public void setItemSpec(String itemSpec) {
        this.itemSpec = itemSpec;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public String getFirstUnit() {
        return firstUnit;
    }

    public void setFirstUnit(String firstUnit) {
        this.firstUnit = firstUnit;
    }

    public String getFirstCount() {
        return firstCount;
    }

    public void setFirstCount(String firstCount) {
        this.firstCount = firstCount;
    }

    public String getSecondUnit() {
        return secondUnit;
    }

    public void setSecondUnit(String secondUnit) {
        this.secondUnit = secondUnit;
    }

    public String getSecondCount() {
        return secondCount;
    }

    public void setSecondCount(String secondCount) {
        this.secondCount = secondCount;
    }

}
