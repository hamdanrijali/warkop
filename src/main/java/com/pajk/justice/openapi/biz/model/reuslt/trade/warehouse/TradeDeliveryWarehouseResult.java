package com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeDeliveryWarehouseResult extends BaseResult {

    private static final long serialVersionUID = -6343051867855743395L;

    public TradeDeliveryWarehouseResult() {
    }

    public TradeDeliveryWarehouseResult(Integer code, String msg) {
        super(code, msg);
    }

}
