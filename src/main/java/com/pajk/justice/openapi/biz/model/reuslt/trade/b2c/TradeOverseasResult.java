package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by FUYONGDA615 on 2019/3/19.
 * Modified by FUYONGDA615 on 2019/3/19.
 */
public class TradeOverseasResult extends BaseResult {

    private static final long serialVersionUID = 3954828757447559619L;

    /**
     * 运输方式 1:直邮 2:保税仓
     */
    private String htShippingType;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 是否可申报标识 0:不能申报 1:可申报
     */
    private String declareFlag;

    /**
     * 平台代码 写死PAJK-HYS
     */
    private String storeCode;

    /**
     * 平台海关备案编码
     */
    private String customsNo;

    /**
     * 平台海关备案名称
     */
    private String customsName;

    /**
     * 商户海关备案编码
     */
    private String sellerCustomsNo;

    /**
     * 商户海关备案名称
     */
    private String sellerCustomsName;

    /**
     * 支付公司海关备案编码
     */
    private String paymentCustomsNo;

    /**
     * 支付公司海关备案名称
     */
    private String paymentCustomsName;

    /**
     * 支付类型 01:银行卡 02:余额支付 03:其他 写死03
     */
    private String payType;

    /**
     * 购买人信息
     */
    private TradeBuyerResult buyer;

    public String getHtShippingType() {
        return htShippingType;
    }

    public void setHtShippingType(String htShippingType) {
        this.htShippingType = htShippingType;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getDeclareFlag() {
        return declareFlag;
    }

    public void setDeclareFlag(String declareFlag) {
        this.declareFlag = declareFlag;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getCustomsNo() {
        return customsNo;
    }

    public void setCustomsNo(String customsNo) {
        this.customsNo = customsNo;
    }

    public String getCustomsName() {
        return customsName;
    }

    public void setCustomsName(String customsName) {
        this.customsName = customsName;
    }

    public String getSellerCustomsNo() {
        return sellerCustomsNo;
    }

    public void setSellerCustomsNo(String sellerCustomsNo) {
        this.sellerCustomsNo = sellerCustomsNo;
    }

    public String getSellerCustomsName() {
        return sellerCustomsName;
    }

    public void setSellerCustomsName(String sellerCustomsName) {
        this.sellerCustomsName = sellerCustomsName;
    }

    public String getPaymentCustomsNo() {
        return paymentCustomsNo;
    }

    public void setPaymentCustomsNo(String paymentCustomsNo) {
        this.paymentCustomsNo = paymentCustomsNo;
    }

    public String getPaymentCustomsName() {
        return paymentCustomsName;
    }

    public void setPaymentCustomsName(String paymentCustomsName) {
        this.paymentCustomsName = paymentCustomsName;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public TradeBuyerResult getBuyer() {
        return buyer;
    }

    public void setBuyer(TradeBuyerResult buyer) {
        this.buyer = buyer;
    }

}
