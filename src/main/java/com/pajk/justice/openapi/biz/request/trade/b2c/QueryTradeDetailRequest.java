package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.QueryTradeDetailResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class QueryTradeDetailRequest  implements IRequest<QueryTradeDetailResult> {
    private static final String apiGroup = "shennong";
    private static final String apiName = "queryTradeDetail";
    private static final String apiId = "d2d3eb1f3286bd2a9f1d9b77aa35d3b3";

    private Long sellerId;
    private String tradeId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("tradeId",tradeId);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<QueryTradeDetailResult> getClassName() {
        return QueryTradeDetailResult.class;
    }


}
