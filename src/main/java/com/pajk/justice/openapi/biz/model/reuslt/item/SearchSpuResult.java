package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SearchSpuResult extends PageResult {
    private static final long serialVersionUID = 2918262718060794325L;

    public List<ItemSearchSpuResult> model;

    public List<ItemSearchSpuResult> getModel() {
        return model;
    }

    public void setModel(List<ItemSearchSpuResult> model) {
        this.model = model;
    }
}
