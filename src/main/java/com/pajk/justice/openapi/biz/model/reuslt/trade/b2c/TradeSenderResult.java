package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeSenderResult extends BaseDO {

    private static final long serialVersionUID = 4401787416559405306L;

    /**
     * 发件人姓名
     */
    private String name;

    /**
     * 发件人电话
     */
    private String phone;

    /**
     * 发件人详细地址
     */
    private String address;

    /**
     * 发件人地址所在身份
     */
    private String prov;

    /**
     * 发件人地址所在城市
     */
    private String city;

    /**
     * 发件人地址所在区
     */
    private String area;

    /**
     * 发件人邮编
     */
    private String zipCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
