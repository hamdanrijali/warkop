package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.ReceivedRefundResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class ReceivedRefundRequest implements IRequest<ReceivedRefundResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "receivedRefund";
    private static final String apiId = "44c284ba6d16a88bc8c195d20503b4ad";

    private Long sellerId;
    private String refundId;
    private String prov;
    private String city;
    private String area;
    private String address;
    private String name;
    private String phone;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("refundId",refundId);
        map.put("prov",prov);
        map.put("city",city);
        map.put("prov",prov);
        map.put("area",area);
        map.put("address",address);
        map.put("name",name);
        map.put("phone",phone);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<ReceivedRefundResult> getClassName() {
        return ReceivedRefundResult.class;
    }

}
