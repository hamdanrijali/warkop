package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class QuerySkuByOuterSkuIdResult extends BaseResult {

    private static final long serialVersionUID = -2370116268526063477L;

    private List<ItemSkuResult> model;

    public List<ItemSkuResult> getModel() {
        return model;
    }

    public void setModel(List<ItemSkuResult> model) {
        this.model = model;
    }
}
