package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.CancelOrderResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.DeliverGoodsResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * Created by yangguoqing on 2018/7/24.
 *
 * @author yangguoqing
 */
public class CancelOrderRequest implements IRequest<CancelOrderResult> {

    String apiGroup = "shennong";
    String apiName = "cancelOrder";
    String apiId = "d8d471685615b74b9ec36a2f20b88cc9";

    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 订单ID（必填）
     */
    private String tradeId;

    /**
     * 取消原因（必填）
     */
    private String reason;

    /**
     * 取消原因code（必填）
     */
    private Integer reasonCode;

    /**
     * 门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private Long storeId;

    /**
     * 外部门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private String outStoreId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("tradeId", tradeId);
        map.put("storeId", storeId);
        map.put("outStoreId", outStoreId);
        map.put("reason", reason);
        map.put("reasonCode", reasonCode);

        list.add(map);
        return list;

    }
    @Override
    public Class<CancelOrderResult> getClassName() {
        return CancelOrderResult.class;
    }
}
