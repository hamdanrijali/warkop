package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by zhangfeng2 on 2019/1/18.
 */
public class QueryDrugSuggestionResult extends BaseResult {

    private static final long serialVersionUID = 7496174229887570027L;

    private DrugSuggestionResult model;

    public DrugSuggestionResult getModel() {
        return model;
    }

    public void setModel(DrugSuggestionResult model) {
        this.model = model;
    }

    public QueryDrugSuggestionResult() {

    }

    public QueryDrugSuggestionResult(int code, String msg) {

    }
}
