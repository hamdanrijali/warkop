package com.pajk.justice.openapi.biz.model.reuslt.base;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua on 2018/4/11.
 * @since v1.0.0
 */
public class BaseResult extends BaseDO {
    private static final long serialVersionUID = 19040755348300161L;

    private Integer resultCode;
    private String resultMsg;

    public BaseResult(){
        this.resultCode = 0;
        this.resultMsg = "成功";
    }

    public BaseResult(Integer code, String msg) {
        this.resultCode = code;
        this.resultMsg = msg;
    }


    public boolean isSuccess(){
        return 0 == resultCode;
    }

    public int getResultCode() {
        return this.resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return this.resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
