package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.ExportCategoryBySellerIdResult;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindCategoryPropByCatIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ExportCategoryBySellerIdRequest extends BaseRequest implements IRequest<ExportCategoryBySellerIdResult> {

    private static final long serialVersionUID = 9026424584146720357L;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "08817f4401ac96321c8cf48f8f0df696";
    }

    @Override
    public String getApiName() {
        return "exportCategoryBySellerId";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<ExportCategoryBySellerIdResult> getClassName() {
        return ExportCategoryBySellerIdResult.class;
    }

}
