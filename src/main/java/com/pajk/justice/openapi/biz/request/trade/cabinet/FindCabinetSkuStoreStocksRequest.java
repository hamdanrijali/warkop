package com.pajk.justice.openapi.biz.request.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet.FindCabinetSkuStoreStocksResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FindCabinetSkuStoreStocksRequest implements IRequest<FindCabinetSkuStoreStocksResult> {

    private String apiId = "7ddf31f51a6e83853feaaf227f8bddf5";
    private String apiGroup = "shennong";
    private String apiName = "findCabinetSkuStoreStocks";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * skuId 列表
     */
    private List<Long> skuIds;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public List<Long> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Long> skuIds) {
        this.skuIds = skuIds;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("storeId", storeId);
        map.put("skuIds", skuIds);
        list.add(map);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<FindCabinetSkuStoreStocksResult> getClassName() {
        return FindCabinetSkuStoreStocksResult.class;
    }
}
