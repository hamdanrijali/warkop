package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.FindSkuIdBySellerIdAndOuterIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class FindSkuIdBySellerIdAndOuterIdRequest implements IRequest<FindSkuIdBySellerIdAndOuterIdResult> {

    String apiGroup = "shennong";
    String apiName = "findSkuIdBySellerIdAndOuterId";
    String apiId = "9bb2ce64b32f5dd1faac57fb99ec3e84";


    private Long sellerId;
    private List<String> skuOuterIds;


    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public List<String> getSkuOuterIds() {
        return skuOuterIds;
    }

    public void setSkuOuterIds(List<String> skuOuterIds) {
        this.skuOuterIds = skuOuterIds;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("skuOuterIds",skuOuterIds);

        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<FindSkuIdBySellerIdAndOuterIdResult> getClassName() {
        return FindSkuIdBySellerIdAndOuterIdResult.class;
    }
}
