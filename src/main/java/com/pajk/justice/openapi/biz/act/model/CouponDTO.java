package com.pajk.justice.openapi.biz.act.model;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author DENGCHUHUA127 on 2018/11/23.
 * @since v2.4.0
 */
public class CouponDTO  extends BaseDO{

    private static final long serialVersionUID = -7471515430630374182L;
    private long  userId;
    private String tradeUuid;
    private String prizeCode;
    /**
     * 优惠金额（单位：分）
     */
    private String couponAmount;
    /**
     * 使用门槛价格（单位：分）
     */
    private String thresholdPrice;

    private String startTime;

    private String endTime;

    private Integer effectiveDays;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTradeUuid() {
        return tradeUuid;
    }

    public void setTradeUuid(String tradeUuid) {
        this.tradeUuid = tradeUuid;
    }

    public String getPrizeCode() {
        return prizeCode;
    }

    public void setPrizeCode(String prizeCode) {
        this.prizeCode = prizeCode;
    }

    public String getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        this.couponAmount = couponAmount;
    }

    public String getThresholdPrice() {
        return thresholdPrice;
    }

    public void setThresholdPrice(String thresholdPrice) {
        this.thresholdPrice = thresholdPrice;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getEffectiveDays() {
        return effectiveDays;
    }

    public void setEffectiveDays(Integer effectiveDays) {
        this.effectiveDays = effectiveDays;
    }
}
