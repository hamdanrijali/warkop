package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/28.
 * Modify by fanhuafeng on 18/4/28
 */
public class QueryPrescriptionFileResult extends BaseResult {
    private static final long serialVersionUID = -5912259322091561476L;

    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public QueryPrescriptionFileResult() {
    }

    public QueryPrescriptionFileResult(int code, String msg) {
        super(code, msg);
    }
}
