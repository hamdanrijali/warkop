package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class SearchItemResult extends PageResult {

    private static final long serialVersionUID = -1845838032338470001L;
    private List<ItemSearchDTO> model;

    public List<ItemSearchDTO> getModel() {
        return model;
    }

    public void setModel(List<ItemSearchDTO> model) {
        this.model = model;
    }
}
