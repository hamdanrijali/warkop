package com.pajk.justice.openapi.biz.request.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet.UpdateCabinetStockResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UpdateCabinetStockRequest implements IRequest<UpdateCabinetStockResult> {

    private String apiId = "59595880c3fe7cd7651179e9ba524070";
    private String apiGroup = "shennong";
    private String apiName = "updateCabinetStock";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * sku id
     */
    private Long skuId;

    /**
     * 库存
     */
    private Integer stock;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("storeId", storeId);
        map.put("skuId", skuId);
        map.put("stock", stock);
        list.add(map);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<UpdateCabinetStockResult> getClassName() {
        return UpdateCabinetStockResult.class;
    }
}
