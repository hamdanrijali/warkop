package com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.StoreTradeResult;

/**
 * Created by fanhuafeng on 18/4/28.
 * Modify by fanhuafeng on 18/4/28
 */
public class QueryCabinetSingleTradeResult extends BaseResult {
    private static final long serialVersionUID = -7763802025466260999L;

    private StoreTradeResult model;

    public StoreTradeResult getModel() {
        return model;
    }

    public void setModel(StoreTradeResult model) {
        this.model = model;
    }

    public QueryCabinetSingleTradeResult() {
    }

    public QueryCabinetSingleTradeResult(int code, String msg) {
        super(code, msg);
    }
}
