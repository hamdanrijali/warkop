package com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;
import com.pajk.justice.openapi.biz.model.reuslt.item.CabinetSkuStoreResult;

import java.util.List;

public class FindCabinetSkuStoreStocksResult extends BaseResult {

    private static final long serialVersionUID = -5657163419612320213L;

    private List<CabinetSkuStoreResult> model;

    public List<CabinetSkuStoreResult> getModel() {
        return model;
    }

    public void setModel(List<CabinetSkuStoreResult> model) {
        this.model = model;
    }

    public FindCabinetSkuStoreStocksResult() {}

    public FindCabinetSkuStoreStocksResult(int code, String msg) {
        super(code, msg);
    }
}
