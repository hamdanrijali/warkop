package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class RefundItemResult extends BaseDO{

    private static final long serialVersionUID = -3105303720810503424L;

    /**
     * 平安商品spuId
     */
    private Long spuId;

    /**
     * 平安商品skuId
     */
    private Long skuId;

    /**
     * 对应外部商家商品sku编号
     */
    private String outSkuId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 购买数量
     */
    private Integer buyCount;

    /**
     * 退款数量
     */
    private Integer refundCount;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(Integer buyCount) {
        this.buyCount = buyCount;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }
}
