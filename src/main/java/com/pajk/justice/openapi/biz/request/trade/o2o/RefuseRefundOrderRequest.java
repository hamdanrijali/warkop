package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.RefuseRefundOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RefuseRefundOrderRequest implements IRequest<RefuseRefundOrderResult> {
    String apiGroup = "shennong";
    String apiName = "refuseRefundOrder";
    String apiId = "540a4b7d107cc5e1cdffc46e08f6bec3";

    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 退款单ID（必填）
     */
    private String refundId;

    /**
     * 取消原因（必填）
     * 长度不能超过100
     */
    private String reason;

    /**
     * 门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private Long storeId;

    /**
     * 外部门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private String outStoreId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("refundId", refundId);
        map.put("reason", reason);
        map.put("storeId", storeId);
        map.put("outStoreId", outStoreId);
        list.add(map);
        return list;

    }

    @Override
    public Class<RefuseRefundOrderResult> getClassName() {
        return RefuseRefundOrderResult.class;
    }

}
