package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindItemByIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindItemByIdRequest extends BaseRequest implements IRequest<FindItemByIdResult> {

    private static final long serialVersionUID = 4519191562968567081L;
    private Long id;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "ee1fd5b2a163a1c6885a50331e6206ce";
    }

    @Override
    public String getApiName() {
        return "findItemById";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<FindItemByIdResult> getClassName() {
        return FindItemByIdResult.class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
