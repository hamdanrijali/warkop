package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class AcceptRefundResult extends BaseResult {
    private static final long serialVersionUID = 9147921111573662016L;

    public AcceptRefundResult() {
    }

    public AcceptRefundResult(int code, String msg) {
        super(code, msg);
    }

}