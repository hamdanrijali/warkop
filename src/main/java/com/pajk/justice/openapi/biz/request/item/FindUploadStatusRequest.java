package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindSkuByIdResult;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindUploadStatusResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindUploadStatusRequest extends BaseRequest implements IRequest<FindUploadStatusResult> {

    private static final long serialVersionUID = 7470817825783942084L;
    private String outerId;
    /**
     */
    private Integer bizType;
    /** 序号*/
    private Integer position;
    /**
     */
    private Integer status;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("outerId",outerId);
        map.put("bizType",bizType);
        map.put("position",position);
        map.put("status",status);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "ee5a05eba84d1eb843b0634e17b288b9";
    }

    @Override
    public String getApiName() {
        return "findUploadStatus";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<FindUploadStatusResult> getClassName() {
        return FindUploadStatusResult.class;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId;
    }

    public Integer getBizType() {
        return bizType;
    }

    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
