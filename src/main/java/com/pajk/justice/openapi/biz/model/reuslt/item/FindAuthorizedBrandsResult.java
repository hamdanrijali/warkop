package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.PageResult;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindAuthorizedBrandsResult  extends PageResult {


    private static final long serialVersionUID = 2601741072594765036L;
    private List<BrandDTO> model;

    public List<BrandDTO> getModel() {
        return model;
    }

    public void setModel(List<BrandDTO> model) {
        this.model = model;
    }
}
