package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;
import java.util.Map;

public class OrderResult extends BaseDO {

    private static final long serialVersionUID = 8627046283614270503L;

    /**
     * 订单号
     */
    private String tradeId;

    /**
     * 卖家id
     */
    private Long sellerId;

    /**
     * (平安)门店id
     */
    private Long storeId;

    /**
     * 外部门店id(即商户门店id)
     */
    private String outStoreId;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String createdTime;

    /**
     * 修改时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String modifiedTime;

    /**
     * 支付时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String payTime;

    /**
     * 实际支付现金(包含邮费)，单位:分
     */
    private Long actualCash;

    /**
     * 实际运费，单位:分
     */
    private Long logisticsFee;

    /**
     * 原始邮费，单位:分
     */
    private Long origLogisticsFee;

    /**
     * 业务渠道
     */
    private String subBizType;

    /**
     * 支付方式
     */
    private String payMode;

    /**
     * 配送方式
     */
    private String logisticsMode;

    /**
     * 交易收款方
     */
    private String payToType;

    /**
     * 开票方
     */
    private String invoiceToType;

    /**
     * 商户订单备注
     */
    private String sellerRemark;

    /**
     * 买家订单备注
     */
    private String buyerRemark;

    /**
     * 商品信息
     */
    private List<ItemResult> items;

    /**
     * 收货人信息
     */
    private LogisticsResult logistics;

    /**
     * 支付信息
     */
    private List<PaymentResult> payments;

    /**
     * 发票信息
     */
    private InvoiceResult invoice;

    /**
     * 扩展信息
     */
    private Map<String, String> props;

    /**
     * 骑手取货码
     */
    private String riderVerifyCode;

    /**
     * O2O订单类型 ("central" 中心仓 "normal" 普通O2O订单)
     */
    private String o2oBizType;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public Long getActualCash() {
        return actualCash;
    }

    public void setActualCash(Long actualCash) {
        this.actualCash = actualCash;
    }

    public Long getLogisticsFee() {
        return logisticsFee;
    }

    public void setLogisticsFee(Long logisticsFee) {
        this.logisticsFee = logisticsFee;
    }

    public Long getOrigLogisticsFee() {
        return origLogisticsFee;
    }

    public void setOrigLogisticsFee(Long origLogisticsFee) {
        this.origLogisticsFee = origLogisticsFee;
    }

    public String getSubBizType() {
        return subBizType;
    }

    public void setSubBizType(String subBizType) {
        this.subBizType = subBizType;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getLogisticsMode() {
        return logisticsMode;
    }

    public void setLogisticsMode(String logisticsMode) {
        this.logisticsMode = logisticsMode;
    }

    public String getPayToType() {
        return payToType;
    }

    public void setPayToType(String payToType) {
        this.payToType = payToType;
    }

    public String getInvoiceToType() {
        return invoiceToType;
    }

    public void setInvoiceToType(String invoiceToType) {
        this.invoiceToType = invoiceToType;
    }

    public String getSellerRemark() {
        return sellerRemark;
    }

    public void setSellerRemark(String sellerRemark) {
        this.sellerRemark = sellerRemark;
    }

    public String getBuyerRemark() {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark) {
        this.buyerRemark = buyerRemark;
    }

    public List<ItemResult> getItems() {
        return items;
    }

    public void setItems(List<ItemResult> items) {
        this.items = items;
    }

    public LogisticsResult getLogistics() {
        return logistics;
    }

    public void setLogistics(LogisticsResult logistics) {
        this.logistics = logistics;
    }

    public List<PaymentResult> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentResult> payments) {
        this.payments = payments;
    }

    public InvoiceResult getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceResult invoice) {
        this.invoice = invoice;
    }

    public Map<String, String> getProps() {
        return props;
    }

    public void setProps(Map<String, String> props) {
        this.props = props;
    }

    public String getRiderVerifyCode() {
        return riderVerifyCode;
    }

    public void setRiderVerifyCode(String riderVerifyCode) {
        this.riderVerifyCode = riderVerifyCode;
    }

    public String getO2oBizType() {
        return o2oBizType;
    }

    public void setO2oBizType(String o2oBizType) {
        this.o2oBizType = o2oBizType;
    }
}

