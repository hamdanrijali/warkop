package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.AgreeRefundOrderResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.SellerConfirmOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by yangguoqing on 18/7/23.
 * Modify by yangguoqing on 18/7/23
 */
public class AgreeRefundOrderRequest implements IRequest<AgreeRefundOrderResult> {

    String apiGroup = "shennong";
    String apiName = "agreeRefundOrder";
    String apiId = "64d52a3f0258c25c1ff80d613a44a4d1";

    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 退款单ID（必填）
     */
    private String refundId;

    /**
     * 门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private Long storeId;

    /**
     * 外部门店ID（优先使用storeId，如果storeId为空，才使用outStoreId）
     */
    private String outStoreId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("refundId", refundId);
        map.put("storeId", storeId);
        map.put("outStoreId", outStoreId);
        list.add(map);
        return list;
    }

    public Class<AgreeRefundOrderResult> getClassName() {
        return AgreeRefundOrderResult.class;
    }


}
