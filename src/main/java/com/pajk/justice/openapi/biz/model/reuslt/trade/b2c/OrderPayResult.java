package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderPayResult extends BaseDO {

    private static final long serialVersionUID = -7304713318859825120L;


    /**
     * 实际支付运费
     */
    private Long actualPostFee;

    /**
     * 原始邮费
     */
    private Long originalPostFee;

    /**
     * 支付详情
     */
    private List<OrderPayItemResult> payItems;

    public Long getActualPostFee() {
        return actualPostFee;
    }

    public void setActualPostFee(Long actualPostFee) {
        this.actualPostFee = actualPostFee;
    }

    public Long getOriginalPostFee() {
        return originalPostFee;
    }

    public void setOriginalPostFee(Long originalPostFee) {
        this.originalPostFee = originalPostFee;
    }

    public List<OrderPayItemResult> getPayItems() {
        return payItems;
    }

    public void setPayItems(List<OrderPayItemResult> payItems) {
        this.payItems = payItems;
    }
}
