package com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by FUYONGDA615 on 2019/3/13.
 * Modified by FUYONGDA615 on 2019/3/13.
 */
public class TradeWarehouseResult extends BaseDO {

    private static final long serialVersionUID = -9178624111248133975L;

    /**
     * 订单id
     */
    private String tradeId;

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 订单创建时间
     */
    private String createTime;

    /**
     * 最后修改时间
     */
    private String modifyTime;

    /**
     * 是否可申报标识 0:不能申报 1:可申报
     */
    private String declareFlag;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getDeclareFlag() {
        return declareFlag;
    }

    public void setDeclareFlag(String declareFlag) {
        this.declareFlag = declareFlag;
    }

}
