package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderInvoiceResult extends BaseDO {

    private static final long serialVersionUID = -7304713318859825120L;

    /**
     * 发票类型 1.普通发票, 2.增值税发票, 0.没有发票
     */
    private Integer invoiceTypeId = 0;

    /**
     * 发票抬头
     */
    private String invoiceTitle;

    public Integer getInvoiceTypeId() {
        return invoiceTypeId;
    }

    public void setInvoiceTypeId(Integer invoiceTypeId) {
        this.invoiceTypeId = invoiceTypeId;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }
}