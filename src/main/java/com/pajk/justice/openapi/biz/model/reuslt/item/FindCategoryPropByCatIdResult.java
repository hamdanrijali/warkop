package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindCategoryPropByCatIdResult extends BaseResult {
    private static final long serialVersionUID = -4696490581002079283L;
    private List<CategoryPropValuesDTO> model;

    public List<CategoryPropValuesDTO> getModel() {
        return model;
    }

    public void setModel(List<CategoryPropValuesDTO> model) {
        this.model = model;
    }
}
