package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class QuerySelfDeliveryTradeResult extends BaseResult {

    private static final long serialVersionUID = 3480620971910478822L;
    private StoreTradeResult model;

    public StoreTradeResult getModel() {
        return model;
    }

    public void setModel(StoreTradeResult model) {
        this.model = model;
    }


    public QuerySelfDeliveryTradeResult() {

    }

    public QuerySelfDeliveryTradeResult(int code, String msg) {
        super(code, msg);

    }





}
