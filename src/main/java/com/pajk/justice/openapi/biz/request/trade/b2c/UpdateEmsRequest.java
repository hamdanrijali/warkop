package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.UpdateEmsResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class UpdateEmsRequest implements IRequest<UpdateEmsResult> {

    String apiGroup = "shennong";
    String apiName = "updateEMS";
    String apiId = "9d923900e2c909c387efb586090441ad";


    private Long sellerId;
    private String tradeId;
    private Long carrierId;
    private String trackingNumber;

    private String operator = "system";


    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(long carrierId) {
        this.carrierId = carrierId;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        list.add(sellerId);
        list.add(tradeId);
        list.add(carrierId);
        list.add(trackingNumber);
        list.add(operator);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<UpdateEmsResult> getClassName() {
        return UpdateEmsResult.class;
    }


}
