package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class ConfirmDeliveredRefundResult extends BaseResult {
    private static final long serialVersionUID = 9147922222573662016L;

    public ConfirmDeliveredRefundResult() {
    }

    public ConfirmDeliveredRefundResult(int code, String msg) {
        super(code, msg);
    }

}