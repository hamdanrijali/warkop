package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class TradeFullResult extends BaseDO {
    private static final long serialVersionUID = -7993411666520810606L;

    private OrderBizResult bizOrder ;


    private OrderPayResult payOrder;

    private OrderLgResult lgOrder;

    private OrderInvoiceResult invoice;


    public OrderBizResult getBizOrder() {
        return bizOrder;
    }

    public void setBizOrder(OrderBizResult bizOrder) {
        this.bizOrder = bizOrder;
    }

    public OrderPayResult getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(OrderPayResult payOrder) {
        this.payOrder = payOrder;
    }

    public OrderLgResult getLgOrder() {
        return lgOrder;
    }

    public void setLgOrder(OrderLgResult lgOrder) {
        this.lgOrder = lgOrder;
    }

    public OrderInvoiceResult getInvoice() {
        return invoice;
    }

    public void setInvoice(OrderInvoiceResult invoice) {
        this.invoice = invoice;
    }
}


