package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */



public class StoreTradeItemResult extends BaseDO {

    private static final long serialVersionUID = 8214081424328314683L;
    /**
     * 商品SKU ID
     */
    private Long skuId;

    /**
     * 外部商品SKU ID
     */
    private String outSkuId;

    /**
     * 商品售卖数量
     */
    private Integer count;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}


