package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RefundItemResult extends BaseDO {
    private static final long serialVersionUID = 1904075531172000101L;
    /**
     * 商品id
     */
    private String spuId;
    /**
     * 商品skuId
     */
    private String skuId;
    /**
     * 外部合作者商品skuId
     */
    private String outSkuId;
    /**
     * 退的数量
     */
    private String count;
    /**
     * 退商品现金
     */
    private String refundCashFee;
    /**
     * 退现金税费
     */
    private String refundCashTaxationFee;

    public String getSpuId() {
        return spuId;
    }

    public void setSpuId(String spuId) {
        this.spuId = spuId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getRefundCashFee() {
        return refundCashFee;
    }

    public void setRefundCashFee(String refundCashFee) {
        this.refundCashFee = refundCashFee;
    }

    public String getRefundCashTaxationFee() {
        return refundCashTaxationFee;
    }

    public void setRefundCashTaxationFee(String refundCashTaxationFee) {
        this.refundCashTaxationFee = refundCashTaxationFee;
    }
}
