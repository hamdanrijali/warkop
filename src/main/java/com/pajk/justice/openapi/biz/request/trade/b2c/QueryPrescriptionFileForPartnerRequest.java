package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.QueryPrescriptionFileForPartnerResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

public class QueryPrescriptionFileForPartnerRequest implements IRequest<QueryPrescriptionFileForPartnerResult> {
    private static final String apiGroup = "shennong";
    private static final String apiName = "queryPrescriptionFileForPartner";
    private static final String apiId = "b4dd4ece39703453b4c1171022110903";


    private Long sellerId;
    private String tradeId;


    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        list.add(sellerId);
        list.add(tradeId);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<QueryPrescriptionFileForPartnerResult> getClassName() {
        return QueryPrescriptionFileForPartnerResult.class;
    }

}
