package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.reuslt.item.QuerySkuByOuterSkuIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class QuerySkuByOuterSkuIdRequest implements IRequest<QuerySkuByOuterSkuIdResult> {

    String apiGroup = "shennong";
    String apiId = "9154e11e637027bc0a9d7d281b6e2124";
    String apiName = "querySkuByOuterSkuId";

    private Long sellerId;
    private String outerSkuId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getOuterSkuId() {
        return outerSkuId;
    }

    public void setOuterSkuId(String outerSkuId) {
        this.outerSkuId = outerSkuId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        list.add(sellerId);
        list.add(outerSkuId);

        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<QuerySkuByOuterSkuIdResult> getClassName() {
        return QuerySkuByOuterSkuIdResult.class;
    }
}
