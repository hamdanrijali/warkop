package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class TradeCouponResult extends BaseDO {
    private static final long serialVersionUID = 1904075534884000101L;
    /**
     * 支付方式
     */
    private String method;
    /**
     * 对谁支付 1 商品 2 邮费 3 税费
     */
    private String payFor;
    /**
     * 商品id 只有对商品支付 才会该值
     */
    private String skuId;
    /**
     * 抵扣金额
     */
    private String discountFee;
    /**
     * 平台承担比率
     */
    private String platformRatio;
    /**
     * 促销活动或抵用券编号
     */
    private String promRefCode;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPayFor() {
        return payFor;
    }

    public void setPayFor(String payFor) {
        this.payFor = payFor;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(String discountFee) {
        this.discountFee = discountFee;
    }

    public String getPlatformRatio() {
        return platformRatio;
    }

    public void setPlatformRatio(String platformRatio) {
        this.platformRatio = platformRatio;
    }

    public String getPromRefCode() {
        return promRefCode;
    }

    public void setPromRefCode(String promRefCode) {
        this.promRefCode = promRefCode;
    }
}

