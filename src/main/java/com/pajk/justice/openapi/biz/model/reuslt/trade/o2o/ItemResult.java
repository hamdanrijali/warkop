package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class ItemResult extends BaseDO {

    private static final long serialVersionUID = 1204807561619865232L;

    /**
     * 平安商品spuId
     */
    private Long spuId;

    /**
     * 平安商品skuId
     */
    private Long skuId;

    /**
     * 对应外部商家商品sku编号
     */
    private String outSkuId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 购买数量
     */
    private Integer buyCount;

    /**
     * 退款数量
     */
    private Integer refundCount;

    /**
     * 销售单价(分)
     */
    private Long sellPrice;

    /**
     * 商品原价(分)
     */
    private Long origPrice;

    /**
     * 结算价格(默认为0,代表没有结算价格,单位分)
     */
    private Long settlementPrice;

    /**
     * 返点比例(%)，比如“16.0”就代表16%
     */
    private String rebateRatio;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(Integer buyCount) {
        this.buyCount = buyCount;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }

    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Long getOrigPrice() {
        return origPrice;
    }

    public void setOrigPrice(Long origPrice) {
        this.origPrice = origPrice;
    }

    public Long getSettlementPrice() {
        return settlementPrice;
    }

    public void setSettlementPrice(Long settlementPrice) {
        this.settlementPrice = settlementPrice;
    }

    public String getRebateRatio() {
        return rebateRatio;
    }

    public void setRebateRatio(String rebateRatio) {
        this.rebateRatio = rebateRatio;
    }

}
