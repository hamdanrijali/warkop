package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class PropOptionSelection extends BaseDO {

    private static final long serialVersionUID = 1835228825715212421L;
    private Long optionId;
    private String option;
    private Boolean selected;

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public Boolean isSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
