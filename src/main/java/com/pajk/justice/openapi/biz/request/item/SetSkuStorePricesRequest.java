package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.model.query.item.SetSkuStorePricesParam;
import com.pajk.justice.openapi.biz.model.reuslt.item.SetSkuStorePricesResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SetSkuStorePricesRequest  implements IRequest<SetSkuStorePricesResult> {

    String apiGroup = "shennong";
    String apiName = "setSkuStorePrices";
    String apiId = "fddd6b8a683e388ee7b9b53591191366";


    private Long sellerId;

    private Long storeId;

    private String storeOuterId;

    private List<SetSkuStorePricesParam> skuStorePriceList ;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreOuterId() {
        return storeOuterId;
    }

    public void setStoreOuterId(String storeOuterId) {
        this.storeOuterId = storeOuterId;
    }

    public List<SetSkuStorePricesParam> getSkuStorePriceList() {
        return skuStorePriceList;
    }

    public void setSkuStorePriceList(List<SetSkuStorePricesParam> skuStorePriceList) {
        this.skuStorePriceList = skuStorePriceList;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("storeId", storeId);
        map.put("storeOuterId",storeOuterId);
        map.put("skuStorePriceList", skuStorePriceList );
        list.add(map);
        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<SetSkuStorePricesResult> getClassName() {
        return SetSkuStorePricesResult.class;
    }


}
