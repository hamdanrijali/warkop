package com.pajk.justice.openapi.biz.request.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse.TradeFailWarehouseResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeFailWarehouseRequest implements IRequest<TradeFailWarehouseResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "tradeFailWarehouse";
    private static final String apiId = "0f606c4e849259e8bc06d863add49a77";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 订单id
     */
    private String tradeId;

    /**
     * 失败时间(格式:yyyy-MM-dd HH:mm:ss)
     */
    private String time;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 备注
     */
    private String note;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public List<?> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("warehouseId", warehouseId);
        map.put("tradeId", tradeId);
        map.put("time", time);
        map.put("reason", reason);
        map.put("note", note);

        List<Object> list = new LinkedList<>();
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<TradeFailWarehouseResult> getClassName() {
        return TradeFailWarehouseResult.class;
    }

}
