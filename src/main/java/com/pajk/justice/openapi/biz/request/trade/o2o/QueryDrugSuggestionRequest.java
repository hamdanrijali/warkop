package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.QueryDrugSuggestionResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangfeng2 on 2019/1/18.
 */
public class QueryDrugSuggestionRequest implements IRequest<QueryDrugSuggestionResult> {

    String apiGroup = "shennong";
    String apiName = "queryDrugSuggestion";
    String apiId = "58ebd699a126ee399cf6b727880e1d16";

    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 订单ID（必填）
     */
    private String tradeId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }


    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("tradeId", tradeId);

        list.add(map);
        return list;

    }

    @Override
    public Class<QueryDrugSuggestionResult> getClassName() {
        return QueryDrugSuggestionResult.class;
    }
}
