package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ItemSkusDTO extends BaseDO {
    private static final long serialVersionUID = -5654651981707840094L;
    private ItemDTO itemDTO;
    private List<SkuDTO> skuDTOList;
    public ItemSkusDTO(){

    }
    public ItemSkusDTO(ItemDTO itemDTO, List<SkuDTO> skuDTOList) {
        this.itemDTO = itemDTO;
        this.skuDTOList = skuDTOList;
    }

    public ItemDTO getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(ItemDTO itemDTO) {
        this.itemDTO = itemDTO;
    }

    public List<SkuDTO> getSkuDTOList() {
        return skuDTOList;
    }

    public void setSkuDTOList(List<SkuDTO> skuDTOList) {
        this.skuDTOList = skuDTOList;
    }
}
