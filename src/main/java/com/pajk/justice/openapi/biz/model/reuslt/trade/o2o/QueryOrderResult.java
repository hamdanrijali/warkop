package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class QueryOrderResult extends BaseResult {

    private static final long serialVersionUID = 8627046283614270503L;

    private OrderResult model;

    public OrderResult getModel() {
        return model;
    }

    public void setModel(OrderResult model) {
        this.model = model;
    }


    public QueryOrderResult() {

    }

    public QueryOrderResult(int code, String msg) {
        super(code, msg);

    }


}

