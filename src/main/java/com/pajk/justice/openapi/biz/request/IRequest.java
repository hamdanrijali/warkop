package com.pajk.justice.openapi.biz.request;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/28.
 * Modify by fanhuafeng on 18/4/28
 */
public interface IRequest<T>  {


    List<?> getData();
    String getApiId();
    String getApiName();
    String getApiGroup();


    Class<T> getClassName();
}
