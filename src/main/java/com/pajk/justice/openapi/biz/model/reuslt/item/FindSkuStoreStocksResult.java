package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class FindSkuStoreStocksResult extends BaseResult {

    private static final long serialVersionUID = 9013815089932159112L;

    private List<SkuStoreStockResult> model;

    public List<SkuStoreStockResult> getModel() {
        return model;
    }

    public void setModel(List<SkuStoreStockResult> model) {
        this.model = model;
    }
}
