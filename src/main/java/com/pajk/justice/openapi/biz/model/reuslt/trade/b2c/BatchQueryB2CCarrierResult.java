package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class BatchQueryB2CCarrierResult extends BaseResult {
    private static final long serialVersionUID = 3446135917483862287L;


    private List<CarrierResult> model;


    public List<CarrierResult> getModel() {
        return model;
    }

    public void setModel(List<CarrierResult> model) {
        this.model = model;
    }

    public BatchQueryB2CCarrierResult() {
    }

    public BatchQueryB2CCarrierResult(int code, String msg) {
        super(code, msg);
    }
}
