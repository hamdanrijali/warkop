package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class ItemSkuResult extends BaseDO {
    private static final long serialVersionUID = -7023959473536942585L;

    private Long	spuId;
    private Long	skuId;
    private String	outerSkuId;
    private Integer	status;
    private Long	sellPrice;
    private Long	origPrice;
    private String	pictureUrl;
    private String	isLockStock;
    private String	isLockPrice;
    private String	specValues;
    private String	createdTime;
    private String	modifiedTime;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getOuterSkuId() {
        return outerSkuId;
    }

    public void setOuterSkuId(String outerSkuId) {
        this.outerSkuId = outerSkuId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Long getOrigPrice() {
        return origPrice;
    }

    public void setOrigPrice(Long origPrice) {
        this.origPrice = origPrice;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getIsLockStock() {
        return isLockStock;
    }

    public void setIsLockStock(String isLockStock) {
        this.isLockStock = isLockStock;
    }

    public String getIsLockPrice() {
        return isLockPrice;
    }

    public void setIsLockPrice(String isLockPrice) {
        this.isLockPrice = isLockPrice;
    }

    public String getSpecValues() {
        return specValues;
    }

    public void setSpecValues(String specValues) {
        this.specValues = specValues;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
