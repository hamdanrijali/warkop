package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class ReceivedRefundResult extends BaseResult {
    private static final long serialVersionUID = 9147933333573662016L;

    public ReceivedRefundResult() {
    }

    public ReceivedRefundResult(int code, String msg) {
        super(code, msg);
    }
}