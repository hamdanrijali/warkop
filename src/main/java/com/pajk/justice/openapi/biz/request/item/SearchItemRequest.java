package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.base.PageRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindItemByIdResult;
import com.pajk.justice.openapi.biz.model.reuslt.item.SearchItemResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class SearchItemRequest extends PageRequest implements IRequest<SearchItemResult> {

    private static final long serialVersionUID = -3991771949849589831L;
    /** 商品标题(支持模糊搜索 ，title 和 categoryId要求至少传一个)如需不需要此字段纳入查询 则传入" * "*/
    private String title;
    /** 3级类目id */
    private Long categoryId;
    /** 上下架状态 */
    private Integer status;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("title",title);
        map.put("categoryId",categoryId);
        map.put("status",status);
        map.put("needCount",needCount);
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "3135d13170c82a32c95d4283c0157135";
    }

    @Override
    public String getApiName() {
        return "searchItem";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<SearchItemResult> getClassName() {
        return SearchItemResult.class;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
