package com.pajk.justice.openapi.biz.common;

import com.alibaba.fastjson.JSON;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;
import com.pajk.justice.openapi.biz.model.reuslt.base.QueryResult;
import com.pajk.justice.openapi.biz.request.IRequest;
import com.pajk.openapi.codec.client.RequestEncoder;
import com.pajk.openapi.codec.client.RequestEntity;
import com.pajk.openapi.codec.client.ResponseDecoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

/**
 * Created by fanhuafeng on 18/4/28.
 * Modify by fanhuafeng on 18/4/28
 */
public class JkClient {

    /**
     * 接口设置是否需要打印;
     */
    private Boolean printFlag ;


    /**
     * 设置连接主机超时（单位：毫秒） 默认5000毫秒
     */
    private int connectTimeOut = 5000;
    /**
     * 设置从主机读取数据超时（单位：毫秒）默认5000毫秒
     */
    private int readTimeOut = 5000;


    private QueryEnv env;
    private String partnerId;
    private String key;


    public JkClient(String partnerId, String key, QueryEnv env) {
        this.env = env;
        this.partnerId = partnerId;
        this.key = key;
    }


    public void setPrintFlag(Boolean printFlag) {
        this.printFlag = printFlag;
    }


    public <T extends BaseResult> T execute(IRequest<T> request)   {
        RequestEncoder encoder = build(request.getApiId());
        if(null != request.getData()) {
            for (Object item : request.getData()) {
                encoder.addParameter(item);
            }
        }
        RequestEntity e = encoder.encode();
        String postURL = getUrl(request.getApiGroup(), request.getApiName(), e.getQueryParams());
        String postData = e.getFormParams();
        QueryResult result = query(postURL, postData);

        if(!result.isSuccess()){
            BaseResult baseResult = new BaseResult(result.getResultCode(), result.getResultMsg());
            result.setModel(JSON.toJSONString(baseResult));
        }

        return JSON.parseObject(result.getModel(),  request.getClassName());
    }



    protected RequestEncoder build(String apiId){
        return new RequestEncoder(partnerId, key, getQueryApiId(apiId));
    }


    protected String getUrl(String apiGroup ,String apiName, String param){
        return env.getUrl() +  apiGroup + "/" + apiName +  "?" + param;
    }

    protected boolean needPrint(){
        if(null == printFlag){
            return env.getPrintFlag();
        }else {
            return printFlag;
        }
    }


    /**
     * 商城OpenApi 对接Demo 返回值解码
     * @param postURL       请求URL
     * @param postData      请求数据
     * @return ResultDTO<String>    网络请求结果
     */
    protected QueryResult query(String postURL, String postData){
        if(needPrint()) {
            System.out.println("\n");
            System.out.println("==http url :" + postURL);
            System.out.println("==http data:" + postData);
            System.out.println("\n");
        }
        //post "application/x-www-form-urlencoded" 请求
        QueryResult result  = doHttpQuery(postURL, postData);

        if(!result.isSuccess()){
            return result;
        }

        // 解析返回值
        QueryResult result1 = new QueryResult();

        Map obj = JSON.parseObject(result.getModel(), Map.class);

        if (0 != (int) obj.get("code")){
            result1 = new QueryResult(-110, "解析api网关错误， 错误码："+obj.get("code") + "， 错误信息："
                    + obj.get("message") + ", " + obj.get("tips") );
            if(needPrint()){
                System.out.println("\n");
                System.out.println("==result:"+ result1);
                System.out.println("\n");

            }
            return result1;

        }
        ResponseDecoder decoder = new ResponseDecoder(key);
        decoder.decode(obj.get("object").toString());
        result1.setModel(decoder.getData());
        if(needPrint()){
            System.out.println("\n");
            System.out.println("==result:"+ result1);
            System.out.println("\n");
        }
        return result1;
    }

    private QueryResult doHttpQuery(String url_address, String request_body) {

        try{
            // Configure and open a connection to the site you will send the request
            URL url = new URL(url_address);
            URLConnection urlConnection = url.openConnection();
            // 设置doOutput属性为true表示将使用此urlConnection写入数据
            urlConnection.setDoOutput(true);
            // 定义待写入数据的内容类型，我们设置为application/x-www-form-urlencoded类型
            urlConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            urlConnection.setConnectTimeout(connectTimeOut);
            urlConnection.setReadTimeout(readTimeOut);
            // 得到请求的输出流对象
            // 把数据写入请求的Body
            try (OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());){
                out.write(request_body);
                out.flush();
            }catch (IOException e){
                return new QueryResult(-100, String.format("IOException:%s",e.toString()));
            }
            // 从服务器读取响应
            return ConvertStream2Json(urlConnection.getInputStream());
        }catch(IOException e){
            return new QueryResult(-100, String.format("IOException:%s",e.toString()));
        }

    }

    private QueryResult ConvertStream2Json(InputStream inputStream)
    {
        String jsonStr;
        // 将输入流转移到内存输出流中
        try(ByteArrayOutputStream out = new ByteArrayOutputStream()){
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer, 0, buffer.length)) != -1){
                out.write(buffer, 0, len);
            }
            // 将内存流转换为字符串
            jsonStr = new String(out.toByteArray());
            QueryResult result = new QueryResult();
            result.setModel(jsonStr);
            return result;
        }
        catch (IOException e){
            return new QueryResult(-101, "IOException"+e.toString());
        }

    }

    private String getQueryApiId(String apiId){
        return apiId+"#"+env.getCode();
    }


}
