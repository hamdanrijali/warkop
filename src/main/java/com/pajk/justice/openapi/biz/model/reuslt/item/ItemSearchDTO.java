package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class ItemSearchDTO extends BaseDO {

    private static final long serialVersionUID = -3019215257386225495L;
    /** spuId */
    private Long spuId;
    /** 标题 */
    private String title;
    /** 品牌名称*/
    private String brandName;
    /** sku数量 */
    private Integer skuCount;
    /** 状态： 1：新建；2：上架;  3:  下架*/
    private Integer status;
    /** 是否关联标品*/
    private Boolean isCspu;

    /** 商户id */
    private Long sellerId;
    /** 商户名 */
    private String sellerName;
    /** 商品图片 */
    private String pictures;
    /** 商品类型 */
    private Integer type;
    /** 商品子类型 */
    private Integer subType;
    /** 标题上的规格文案 */
    private String specOnTitle;

    /** 审批状态 */
    private Integer auditStatus;

    /** 厂家id*/
    private Long firmId;
    /** 品牌id*/
    private Long brandId;
    /** 品牌名 */
    private String firmName;

    /** 创建时间*/
    private String createdTime;
    /**最后修改时间 */
    private String modifiedTime;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getSkuCount() {
        return skuCount;
    }

    public void setSkuCount(Integer skuCount) {
        this.skuCount = skuCount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getCspu() {
        return isCspu;
    }

    public void setCspu(Boolean cspu) {
        isCspu = cspu;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public String getSpecOnTitle() {
        return specOnTitle;
    }

    public void setSpecOnTitle(String specOnTitle) {
        this.specOnTitle = specOnTitle;
    }

    public Long getFirmId() {
        return firmId;
    }

    public void setFirmId(Long firmId) {
        this.firmId = firmId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

}
