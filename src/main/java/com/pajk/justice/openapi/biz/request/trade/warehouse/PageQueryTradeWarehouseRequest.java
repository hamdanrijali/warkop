package com.pajk.justice.openapi.biz.request.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse.PageQueryTradeWarehouseResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class PageQueryTradeWarehouseRequest implements IRequest<PageQueryTradeWarehouseResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "pageQueryTradeWarehouse";
    private static final String apiId = "00d3acbd99de422758f77f9edc3c9df5";

    /**
     * 商户id
     */
    private Long sellerId;

    /**
     * 仓库id
     */
    private String warehouseId;

    /**
     * 查询三个月以内订单创建时间-开始(格式:yyyy-MM-dd HH:mm:ss)
     */
    private String startTime;

    /**
     * 查询三个月以内订单创建时间-结束(格式:yyyy-MM-dd HH:mm:ss)
     */
    private String endTime;

    /**
     * 当前页码
     * 0< pageSize <= 100
     * 1 <= pageNo * pageSize <= 10000
     */
    private Integer pageNo;

    /**
     * 每页条数
     * 0 < pageSize <= 100
     * 1 <= pageNo * pageSize <= 10000
     */
    private Integer pageSize;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public List<?> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("warehouseId", warehouseId);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("pageNo", pageNo);
        map.put("pageSize", pageSize);

        List<Object> list = new LinkedList<>();
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<PageQueryTradeWarehouseResult> getClassName() {
        return PageQueryTradeWarehouseResult.class;
    }

}
