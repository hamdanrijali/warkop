package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class VerifyPrescriptionResult extends BaseResult {
    private static final long serialVersionUID = -7152157379168754326L;

    public VerifyPrescriptionResult() {
    }

    public VerifyPrescriptionResult(int code, String msg) {
        super(code, msg);
    }
}
