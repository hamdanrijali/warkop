package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class TradeBuyerResult extends BaseDO {

    private static final long serialVersionUID = 8075724587219130894L;

    /**
     * 购买人ID
     */
    private String id;

    /**
     * 购买人姓名
     */
    private String name;

    /**
     * 购买人电话
     */
    private String phone;

    /**
     * 购买人证件类型
     */
    private String certiType;

    /**
     * 购买人证件号码
     */
    private String certiNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCertiType() {
        return certiType;
    }

    public void setCertiType(String certiType) {
        this.certiType = certiType;
    }

    public String getCertiNo() {
        return certiNo;
    }

    public void setCertiNo(String certiNo) {
        this.certiNo = certiNo;
    }

}
