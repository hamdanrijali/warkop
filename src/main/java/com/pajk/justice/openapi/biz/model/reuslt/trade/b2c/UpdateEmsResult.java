package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class UpdateEmsResult extends BaseResult {
    public UpdateEmsResult() {
    }

    public UpdateEmsResult(int code, String msg) {
        super(code, msg);
    }

    private static final long serialVersionUID = 9147928371573662016L;
}
