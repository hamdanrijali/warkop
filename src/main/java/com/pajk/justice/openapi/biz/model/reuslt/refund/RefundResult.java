package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RefundResult extends BaseDO {
    private static final long serialVersionUID = 1904015534873000101L;
    /**
     * 退款单id
     */
    private String refundId;
    /**
     * 退款单对应的订单id
     */
    private String tradeId;
    /**
     * 退款单状态
     */
    private String status;
    /**
     * 退款单类型
     */
    private String type;
    /**
     * 退款说明
     */
    private String refundComment;
    /**
     * 退款地址 只有退货退款才有
     */
    private String refundAddress;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 最后修改时间
     */
    private String modifyTime;
    /**
     * 退款完结时间
     */
    private String finishTime;
    /**
     * 退款支付渠道
     */
    private String payChannel;
    /**
     * 退款第三方支付流失号
     */
    private String refundExtPayFlow;
    /**
     * 退款平安支付流水号
     */
    private String refundPaPayFlow;
    /**
     * 退款现金邮费
     */
    private String refundCashPostFee;
    /**
     * 退款总现金
     */
    private String refundCashTotalFee;
    /**
     * 扩展属性
     * lgNum 退货的物流单号
     * rejectReason 拒绝原因
     */
    private String extProps;

    private List<RefundItemResult> items = new LinkedList<>();
    private List<RefundCouponResult> coupons = new LinkedList<>();

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRefundComment() {
        return refundComment;
    }

    public void setRefundComment(String refundComment) {
        this.refundComment = refundComment;
    }

    public String getRefundAddress() {
        return refundAddress;
    }

    public void setRefundAddress(String refundAddress) {
        this.refundAddress = refundAddress;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public String getRefundExtPayFlow() {
        return refundExtPayFlow;
    }

    public void setRefundExtPayFlow(String refundExtPayFlow) {
        this.refundExtPayFlow = refundExtPayFlow;
    }

    public String getRefundPaPayFlow() {
        return refundPaPayFlow;
    }

    public void setRefundPaPayFlow(String refundPaPayFlow) {
        this.refundPaPayFlow = refundPaPayFlow;
    }

    public String getRefundCashPostFee() {
        return refundCashPostFee;
    }

    public void setRefundCashPostFee(String refundCashPostFee) {
        this.refundCashPostFee = refundCashPostFee;
    }

    public String getRefundCashTotalFee() {
        return refundCashTotalFee;
    }

    public void setRefundCashTotalFee(String refundCashTotalFee) {
        this.refundCashTotalFee = refundCashTotalFee;
    }

    public String getExtProps() {
        return extProps;
    }

    public void setExtProps(String extProps) {
        this.extProps = extProps;
    }

    public List<RefundItemResult> getItems() {
        return items;
    }

    public void setItems(List<RefundItemResult> items) {
        this.items = items;
    }

    public List<RefundCouponResult> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<RefundCouponResult> coupons) {
        this.coupons = coupons;
    }
}
