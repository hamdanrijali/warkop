package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua on 2018/8/3.
 * @since v1.0.0
 */
public class FileUploadResDTO extends BaseDO {
    private static final long serialVersionUID = 8852064941641743448L;

    /** 外部关联id，spuOuterId或者skuOuterId*/
    private String outerId;
    /**
     * 参见：com.pajk.paridis.enums.FileBizTypeEnum;
     */
    private Integer bizType;
    /** 序号*/
    private Integer position;
    /** 上传时源url*/
    private String sourceURL;
    /**
     * 参见：com.pajk.paridis.enums.TaskStatusEnum;
     */
    private Integer status;

    private String result;
    /** fileKey*/
    private String key;
    /** URL*/
    private String url;

    /** 时间戳 */
    private String timestamp;

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceURL() {
        return sourceURL;
    }

    public void setSourceURL(String sourceURL) {
        this.sourceURL = sourceURL;
    }

    public Integer getBizType() {
        return bizType;
    }

    public void setBizType(Integer bizType) {
        this.bizType = bizType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
