package com.pajk.justice.openapi.biz.model.reuslt.store;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class UpdateStoreStatusResult extends BaseResult {

    private static final long serialVersionUID = -8549481634380927628L;

    public UpdateStoreStatusResult() {
    }

    public UpdateStoreStatusResult(int code, String msg) {
        super(code, msg);
    }
}
