package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindItemAuditResult extends BaseResult {


    private static final long serialVersionUID = -5675360665094778612L;
    private ItemAuditResultDTO model;


    public ItemAuditResultDTO getModel() {
        return model;
    }

    public void setModel(ItemAuditResultDTO model) {
        this.model = model;
    }
}
