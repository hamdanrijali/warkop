package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;


/**
 * Created by lizhijun on 2018/5/29.
 */
public class QueryRefundResult extends BaseResult {
    private static final long serialVersionUID = 5003938362908641108L;
    private RefundResult model;


    public RefundResult getModel() {
        return model;
    }

    public void setModel(RefundResult model) {
        this.model = model;
    }

    public QueryRefundResult() {
    }

    public QueryRefundResult(int code, String msg) {
        super(code, msg);
    }
}
