package com.pajk.justice.openapi.biz.model.reuslt.trade.cabinet;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class UpdateCabinetStockResult extends BaseResult {

    private static final long serialVersionUID = -7060707265874108510L;

    public UpdateCabinetStockResult() {}

    public UpdateCabinetStockResult(int code, String msg) {
        super(code, msg);
    }
}
