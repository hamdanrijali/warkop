package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RejectRefundResult extends BaseResult {
    private static final long serialVersionUID = 9147944444573662016L;

    public RejectRefundResult() {
    }

    public RejectRefundResult(int code, String msg) {
        super(code, msg);
    }
}
