package com.pajk.justice.openapi.biz.act.result;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * @author dengchuhua127 on 2018/11/23.
 * @since v2.4.0
 */
public class IssueHealthGoldResult extends BaseResult {
    private static final long serialVersionUID = -6660967743264904096L;

    public IssueHealthGoldResult() {

    }

    public IssueHealthGoldResult(int code, String msg) {
        super(code, msg);
    }
}
