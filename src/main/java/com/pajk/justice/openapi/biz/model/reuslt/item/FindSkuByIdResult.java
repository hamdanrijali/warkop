package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindSkuByIdResult  extends BaseResult {


    private static final long serialVersionUID = -6108261573819945282L;

    private SkuDTO model;


    public SkuDTO getModel() {
        return model;
    }

    public void setModel(SkuDTO model) {
        this.model = model;
    }
}
