package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SetSkuStoreStocksResult  extends BaseResult {
    private static final long serialVersionUID = 1989086755214669830L;


    private List<String> model;

    public List<String> getModel() {
        return model;
    }

    public void setModel(List<String> model) {
        this.model = model;
    }


    public SetSkuStoreStocksResult() {
    }

    public SetSkuStoreStocksResult(int code, String msg) {
        super(code, msg);
    }
}
