package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

public class PresApprovedResult  extends BaseResult {

    private static final long serialVersionUID = -1732577491906701853L;

    public PresApprovedResult(){
    }

    public PresApprovedResult(int code, String msg){
        super(code, msg);
    }
}
