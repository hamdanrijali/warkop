package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class TradeResult  extends BaseDO {
    private static final long serialVersionUID = 1904075534880000101L;
    /**
     * 订单id
     */
    private String tradeId;
    /**
     * 订单状态
     */
    private String status;
    /**
     * 下单平台
     */
    private String platform;
    /**
     * 业务渠道
     */
    private String subBizType;
    /**
     * 订单分类
     */
    private String orderTag;
    /**
     * 支付方式
     */
    private String payMode;
    /**
     * 在线支付支付时间
     */
    private String payTime;
    /**
     * 订单创建时间
     */
    private String createTime;
    /**
     * 最后修改时间
     */
    private String modifyTime;
    /**
     * 订单原始邮费
     */
    private String originPostFee;
    /**
     * 订单现金邮费
     */
    private String cashPostFee;
    /**
     * 订单现金支付总额
     */
    private String cashTotalFee;
    /**
     * 支付渠道
     */
    private String payChannel;
    /**
     * 第三方支付流水
     */
    private String extPayFlow;
    /**
     * 平安支付流水
     */
    private String paPayFlow;
    /**
     * 对谁支付
     */
    private String payToType;
    /**
     * 审核状态 , 目前只有处方药
     */
    private String approveStatus;
    /**
     * 买家备注
     */
    private String buyerNote;

    /**
     * 订单商品列表
     */
    private List<TradeItemResult> items =  new LinkedList<>();
    /**
     * 订单优惠列表
     */
    private List<TradeCouponResult> coupons = new LinkedList<>();
    /**
     * 订单收货人信息
     */
    private TradeReceiverResult receiver;
    /**
     * 订单发票信息
     */
    private TradeInvoiceResult invoice;


    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSubBizType() {
        return subBizType;
    }

    public void setSubBizType(String subBizType) {
        this.subBizType = subBizType;
    }

    public String getOrderTag() {
        return orderTag;
    }

    public void setOrderTag(String orderTag) {
        this.orderTag = orderTag;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getOriginPostFee() {
        return originPostFee;
    }

    public void setOriginPostFee(String originPostFee) {
        this.originPostFee = originPostFee;
    }

    public String getCashPostFee() {
        return cashPostFee;
    }

    public void setCashPostFee(String cashPostFee) {
        this.cashPostFee = cashPostFee;
    }

    public String getCashTotalFee() {
        return cashTotalFee;
    }

    public void setCashTotalFee(String cashTotalFee) {
        this.cashTotalFee = cashTotalFee;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public String getExtPayFlow() {
        return extPayFlow;
    }

    public void setExtPayFlow(String extPayFlow) {
        this.extPayFlow = extPayFlow;
    }

    public String getPaPayFlow() {
        return paPayFlow;
    }

    public void setPaPayFlow(String paPayFlow) {
        this.paPayFlow = paPayFlow;
    }

    public String getPayToType() {
        return payToType;
    }

    public void setPayToType(String payToType) {
        this.payToType = payToType;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getBuyerNote() {
        return buyerNote;
    }

    public void setBuyerNote(String buyerNote) {
        this.buyerNote = buyerNote;
    }

    public List<TradeItemResult> getItems() {
        return items;
    }

    public void setItems(List<TradeItemResult> items) {
        this.items = items;
    }

    public List<TradeCouponResult> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<TradeCouponResult> coupons) {
        this.coupons = coupons;
    }

    public TradeReceiverResult getReceiver() {
        return receiver;
    }

    public void setReceiver(TradeReceiverResult receiver) {
        this.receiver = receiver;
    }

    public TradeInvoiceResult getInvoice() {
        return invoice;
    }

    public void setInvoice(TradeInvoiceResult invoice) {
        this.invoice = invoice;
    }

}
