package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.AddItemResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class AddItemRequest extends BaseRequest implements IRequest<AddItemResult> {

    private static final long serialVersionUID = 856881999201690452L;

    /** spuOuterId 选填*/
    private String spuOuterId;

    /** 最大购买量 */
    private Integer orderQty;
    /**  三级类目ID */
    private Long categoryId;

    /** 是否商城可见 */
    private Boolean mallVisible;

    /** 厂商名->custom3 */
    private String firmName;

    /** 详情 */
    private String detail;
    /** 品牌ID */
    private Long brandId;
    /** 商品标题 */
    private String title;
    /** 规格 */
    private String specOnTitle;
    /** 商品类型 */
    private Integer type;
    /** 商品子类型 */
    private Integer subType;
    /** 条形码 */
    private String articleNo;
    /** 商品大图 */
    private String pictures;
    /** SKU维度1 */
    private String level1Key;
    /** SKU维度2 */
    private String level2Key;
    /** SKU维度3 */
    private String level3Key;
    /** SKU维度4 */
    private String level4Key;
    /** SKU维度5 */
    private String level5Key;
    /** 邮费模版id*/
    private String postageTemplateId;

//扩展属性 spec
    /** 标题前文案*/
    private String prefixTitle;
    /** 副标题*/
    private String subHeading;
    /** 购买规则*/
    private String shoppingGuide;
    /** 物流提示*/
    private String logisticsTips;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("spuOuterId",spuOuterId);
        map.put("orderQty",orderQty);
        map.put("categoryId",categoryId);
        map.put("isMallVisible", mallVisible);
        map.put("firmName",firmName);
        map.put("detail",detail);
        map.put("brandId",brandId);
        map.put("title",title);
        map.put("specOnTitle",specOnTitle);
        map.put("type",type);
        map.put("subType",subType);
        map.put("articleNo",articleNo);
        map.put("pictures",pictures);

        map.put("level1Key",level1Key);
        map.put("level2Key",level2Key);
        map.put("level3Key",level3Key);
        map.put("level4Key",level4Key);
        map.put("level5Key",level5Key);
        map.put("postageTemplateId",postageTemplateId);
        map.put("prefixTitle",prefixTitle);
        map.put("subHeading",subHeading);
        map.put("shoppingGuide",shoppingGuide);
        map.put("logisticsTips",logisticsTips);

        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "fd4a2e05c01aed6e650b03ba4bcf5cc4";
    }

    @Override
    public String getApiName() {
        return "addItem";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<AddItemResult> getClassName() {
        return AddItemResult.class;
    }

    public String getSpuOuterId() {
        return spuOuterId;
    }

    public void setSpuOuterId(String spuOuterId) {
        this.spuOuterId = spuOuterId;
    }

    public Integer getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Boolean getMallVisible() {
        return mallVisible;
    }

    public void setMallVisible(Boolean mallVisible) {
        this.mallVisible = mallVisible;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSpecOnTitle() {
        return specOnTitle;
    }

    public void setSpecOnTitle(String specOnTitle) {
        this.specOnTitle = specOnTitle;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }


    public String getLevel1Key() {
        return level1Key;
    }

    public void setLevel1Key(String level1Key) {
        this.level1Key = level1Key;
    }

    public String getLevel2Key() {
        return level2Key;
    }

    public void setLevel2Key(String level2Key) {
        this.level2Key = level2Key;
    }

    public String getLevel3Key() {
        return level3Key;
    }

    public void setLevel3Key(String level3Key) {
        this.level3Key = level3Key;
    }

    public String getLevel4Key() {
        return level4Key;
    }

    public void setLevel4Key(String level4Key) {
        this.level4Key = level4Key;
    }

    public String getLevel5Key() {
        return level5Key;
    }

    public void setLevel5Key(String level5Key) {
        this.level5Key = level5Key;
    }

    public String getPostageTemplateId() {
        return postageTemplateId;
    }

    public void setPostageTemplateId(String postageTemplateId) {
        this.postageTemplateId = postageTemplateId;
    }

    public String getPrefixTitle() {
        return prefixTitle;
    }

    public void setPrefixTitle(String prefixTitle) {
        this.prefixTitle = prefixTitle;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public String getShoppingGuide() {
        return shoppingGuide;
    }

    public void setShoppingGuide(String shoppingGuide) {
        this.shoppingGuide = shoppingGuide;
    }

    public String getLogisticsTips() {
        return logisticsTips;
    }

    public void setLogisticsTips(String logisticsTips) {
        this.logisticsTips = logisticsTips;
    }
}
