package com.pajk.justice.openapi.biz.model.reuslt.trade.warehouse;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.TradeDetailResult;

/**
 * Created by FUYONGDA615 on 2019/3/11.
 * Modified by FUYONGDA615 on 2019/3/11.
 */
public class QueryTradeDetailWarehouseResult extends BaseResult {

    private static final long serialVersionUID = -3832546545458532501L;

    private TradeDetailResult model;

    public TradeDetailResult getModel() {
        return model;
    }

    public void setModel(TradeDetailResult model) {
        this.model = model;
    }

    public QueryTradeDetailWarehouseResult() {
    }

    public QueryTradeDetailWarehouseResult(Integer code, String msg) {
        super(code, msg);
    }

}
