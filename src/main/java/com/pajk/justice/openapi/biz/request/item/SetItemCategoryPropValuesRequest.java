package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.CategoryPropValuesDTO;
import com.pajk.justice.openapi.biz.model.reuslt.item.VoidResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class SetItemCategoryPropValuesRequest extends BaseRequest implements IRequest<VoidResult> {

    private static final long serialVersionUID = 1801224989770958612L;
    /** spuId  */
    private Long spuId;

    /** 类目属性和属性值 */
    private List<CategoryPropValuesDTO> categoryPropList;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("spuId",spuId);
        map.put("categoryPropList",categoryPropList);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "7569b036e2618e1598c088613d3f6190";
    }

    @Override
    public String getApiName() {
        return "setItemCategoryPropValues";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<VoidResult> getClassName() {
        return VoidResult.class;
    }

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public List<CategoryPropValuesDTO> getCategoryPropList() {
        return categoryPropList;
    }

    public void setCategoryPropList(List<CategoryPropValuesDTO> categoryPropList) {
        this.categoryPropList = categoryPropList;
    }
}
