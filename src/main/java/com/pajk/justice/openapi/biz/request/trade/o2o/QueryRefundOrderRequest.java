package com.pajk.justice.openapi.biz.request.trade.o2o;

import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.AgreeRefundOrderResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.QueryRefundOrderResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class QueryRefundOrderRequest implements IRequest<QueryRefundOrderResult> {


    String apiGroup = "shennong";
    String apiName = "queryRefundOrder";
    String apiId = "7543af5a046d2849fd7dc65eb231254c";

    /**
     * 商家ID（必填）
     */
    private Long sellerId;

    /**
     * 退款单ID（必填）
     */
    private String refundId;


    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }


    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId", sellerId);
        map.put("refundId", refundId);
        list.add(map);
        return list;
    }

    public Class<QueryRefundOrderResult> getClassName() {
        return QueryRefundOrderResult.class;
    }


}
