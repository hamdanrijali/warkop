package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.PageRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindAuthorizedBrandsResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by dengchuahua on 18/10/18
 *
 */
public class FindAuthorizedBrandsRequest extends PageRequest implements IRequest<FindAuthorizedBrandsResult> {

    private static final long serialVersionUID = -297952062539284976L;

    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<?> getData() {

        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("name",name);
        map.put("needCount",needCount);
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        list.add(map);
        return list;

    }

    @Override
    public String getApiId() {
        return "23ef9bde405483e3c9393c896c99bce7";
    }

    @Override
    public String getApiName() {
        return "findAuthorizedBrands";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }


    @Override
    public Class<FindAuthorizedBrandsResult> getClassName() {
        return FindAuthorizedBrandsResult.class;
    }
}
