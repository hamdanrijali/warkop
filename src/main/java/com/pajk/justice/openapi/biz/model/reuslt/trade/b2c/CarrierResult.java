package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class CarrierResult extends BaseDO {
    private static final long serialVersionUID = -1397181008724310164L;
    private Long carrierId;
    private String carrierNick;

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierNick() {
        return carrierNick;
    }

    public void setCarrierNick(String carrierNick) {
        this.carrierNick = carrierNick;
    }
}
