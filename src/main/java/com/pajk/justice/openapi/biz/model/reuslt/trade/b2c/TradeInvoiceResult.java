package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/17.
 */
public class TradeInvoiceResult extends BaseDO{
    private static final long serialVersionUID = 1904075534883000101L;
    /**
     * 发票模式
     */
    private String mode;
    /**
     * 发票类型
     */
    private String type;
    /**
     * 抬头类型
     */
    private String signal;
    /**
     * 纳税人识别号
     */
    private String taxpayerId;
    /**
     * 抬头内容
     */
    private String title;
    /**
     * 公司名
     */
    private String name;
    /**
     * 公司注册地址
     */
    private String address;
    /**
     * 公司注册电话
     */
    private String phone;
    /**
     * 公司开户银行名称
     */
    private String bankName;
    /**
     * 公司开户银行帐号
     */
    private String bankAccount;
    /**
     * 电子发票通知手机号
     */
    private String receiveMobile;
    /**
     * 电子发票通知邮箱
     */
    private String receiveEmail;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getTaxpayerId() {
        return taxpayerId;
    }

    public void setTaxpayerId(String taxpayerId) {
        this.taxpayerId = taxpayerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getReceiveMobile() {
        return receiveMobile;
    }

    public void setReceiveMobile(String receiveMobile) {
        this.receiveMobile = receiveMobile;
    }

    public String getReceiveEmail() {
        return receiveEmail;
    }

    public void setReceiveEmail(String receiveEmail) {
        this.receiveEmail = receiveEmail;
    }
}
