package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.VoidResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class DelistingItemRequest extends BaseRequest implements IRequest<VoidResult> {

    private static final long serialVersionUID = -1391529103899140649L;
    private Long id;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "a50febe9de3f5a4ad32aaeb4b8c2ec4d";
    }

    @Override
    public String getApiName() {
        return "delistingItem";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<VoidResult> getClassName() {
        return VoidResult.class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
