package com.pajk.justice.openapi.biz.common;

import com.pajk.suez.common.cipher.HexString;
import com.pajk.suez.common.cipher.TripleDESUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class DataDecodeUtil {


    public static String decode(String data, String key){

        if(StringUtils.isEmpty(data)){
            return "input data is null";
        }
        if(StringUtils.isEmpty(key)){
            return "input key is null";
        }

        String sas  = StringUtils.startsWith(data, "q=")? StringUtils.substring(data, 2): data;


        final byte[] plainTextBytes = HexString.toBytes(sas) ;
        try{
            return TripleDESUtil.decrypt(plainTextBytes,key);
        }catch (Exception e){
            return "decode Exception:"+e.toString();
        }

    }
}
