package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

public class InvoiceResult extends BaseDO {

    private static final long serialVersionUID = -420395025763611954L;

    /**
     * 发票类型
     */
    private String type;

    /**
     * 发票抬头
     */
    private String title;

    /**
     * 开票模式
     */
    private String invoiceMode;

    /**
     * 开票人标识
     */
    private String signal;

    /**
     * 购货者(单位)名称
     */
    private String purchaserName;

    /**
     * 购货者(单位)地址
     */
    private String purchaserAddress;

    /**
     * 购货者(单位)电话
     */
    private String purchaserPhone;

    /**
     * 购货者(单位)开户行
     */
    private String purchaserBankName;

    /**
     * 购货者(单位)银行账号
     */
    private String purchaserBankAccount;

    /**
     * 纳税人识别号
     */
    private String taxpayerId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInvoiceMode() {
        return invoiceMode;
    }

    public void setInvoiceMode(String invoiceMode) {
        this.invoiceMode = invoiceMode;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    public String getPurchaserAddress() {
        return purchaserAddress;
    }

    public void setPurchaserAddress(String purchaserAddress) {
        this.purchaserAddress = purchaserAddress;
    }

    public String getPurchaserPhone() {
        return purchaserPhone;
    }

    public void setPurchaserPhone(String purchaserPhone) {
        this.purchaserPhone = purchaserPhone;
    }

    public String getPurchaserBankName() {
        return purchaserBankName;
    }

    public void setPurchaserBankName(String purchaserBankName) {
        this.purchaserBankName = purchaserBankName;
    }

    public String getPurchaserBankAccount() {
        return purchaserBankAccount;
    }

    public void setPurchaserBankAccount(String purchaserBankAccount) {
        this.purchaserBankAccount = purchaserBankAccount;
    }

    public String getTaxpayerId() {
        return taxpayerId;
    }

    public void setTaxpayerId(String taxpayerId) {
        this.taxpayerId = taxpayerId;
    }


}
