package com.pajk.justice.openapi.biz.request.item;

import com.pajk.justice.openapi.biz.common.Constance;
import com.pajk.justice.openapi.biz.model.reuslt.base.BaseRequest;
import com.pajk.justice.openapi.biz.model.reuslt.item.FindSkuByIdResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindSkuByIdRequest extends BaseRequest implements IRequest<FindSkuByIdResult> {

    private static final long serialVersionUID = 856881999201690452L;
    private Long id;

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("id",id);
        list.add(map);
        return list;
    }

    @Override
    public String getApiId() {
        return "34902552a9094b0ebb43ab7e275a9353";
    }

    @Override
    public String getApiName() {
        return "findSkuById";
    }

    @Override
    public String getApiGroup() {
        return Constance.APIGROUP_SHENNONG;
    }

    @Override
    public Class<FindSkuByIdResult> getClassName() {
        return FindSkuByIdResult.class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
