package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

import java.util.List;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class FindUploadStatusResult extends BaseResult {
    private static final long serialVersionUID = -6707355888947450390L;
    private List<FileUploadResDTO> model;

    public List<FileUploadResDTO> getModel() {
        return model;
    }

    public void setModel(List<FileUploadResDTO> model) {
        this.model = model;
    }
}
