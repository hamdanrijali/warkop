package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.PageQueryRefundIncrementResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class PageQueryRefundIncrementRequest implements IRequest<PageQueryRefundIncrementResult> {
    private static final String apiGroup = "shennong";
    private static final String apiName = "pageQueryRefundIncrement";
    private static final String apiId = "d1ffb3e5bc2a3265cd1a1a05f901b2bc";

    private Long sellerId;
    private String startTime;
    private String endTime;
    private String status;
    private Integer pageNo;
    private Integer pageSize;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        map.put("status",status);
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<PageQueryRefundIncrementResult> getClassName() {
        return PageQueryRefundIncrementResult.class;
    }

}
