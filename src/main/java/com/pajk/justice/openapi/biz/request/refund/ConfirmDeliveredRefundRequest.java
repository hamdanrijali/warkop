package com.pajk.justice.openapi.biz.request.refund;

import com.pajk.justice.openapi.biz.model.reuslt.refund.ConfirmDeliveredRefundResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class ConfirmDeliveredRefundRequest implements IRequest<ConfirmDeliveredRefundResult> {

    private static final String apiGroup = "shennong";
    private static final String apiName = "confirmDeliveredRefund";
    private static final String apiId = "2ca79a2fcb89fab1d6442d7a0e6971b1";

    private Long sellerId;
    private String refundId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    @Override
    public List<?> getData() {
        List<Object> list = new LinkedList<>();

        Map<String, Object> map = new HashMap<>();
        map.put("sellerId",sellerId);
        map.put("refundId",refundId);
        list.add(map);

        return list;

    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }


    @Override
    public Class<ConfirmDeliveredRefundResult> getClassName() {
        return ConfirmDeliveredRefundResult.class;
    }

}

