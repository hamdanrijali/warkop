package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class SkuStoreStockResult extends BaseDO {
    private static final long serialVersionUID = -5417228270001369572L;

    private Long skuId;
    /**
     * 可销售库存
     */
    private Integer stockNum;

    /**
     * 总库存
     */
    private Integer totalStockNum;
    /**
     * 冻结库存 = 总库存 - 可销售库存
     */
    private Integer frozenStockNum;


    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getTotalStockNum() {
        return totalStockNum;
    }

    public void setTotalStockNum(Integer totalStockNum) {
        this.totalStockNum = totalStockNum;
    }

    public Integer getFrozenStockNum() {
        return frozenStockNum;
    }

    public void setFrozenStockNum(Integer frozenStockNum) {
        this.frozenStockNum = frozenStockNum;
    }
}
