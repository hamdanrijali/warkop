package com.pajk.justice.openapi.biz.model.reuslt.refund;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by lizhijun on 2018/5/29.
 */
public class RetryRefundResult extends BaseResult {
    private static final long serialVersionUID = 9147955555573662016L;

    public RetryRefundResult() {
    }

    public RetryRefundResult(int code, String msg) {
        super(code, msg);
    }
}
