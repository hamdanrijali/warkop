package com.pajk.justice.openapi.biz.model.reuslt.base;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class PageRequest extends BaseRequest{


    private static final long serialVersionUID = -5384151811672392491L;

    protected int pageNo = 1;
    protected int pageSize = 10;
    protected boolean needCount =true;
    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isNeedCount() {
        return needCount;
    }

    public void setNeedCount(boolean needCount) {
        this.needCount = needCount;
    }
}
