package com.pajk.justice.openapi.biz.model.reuslt.trade.o2o;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

import java.util.List;

public class RefundOrderResult extends BaseDO {

    private static final long serialVersionUID = -4213321558245701950L;

    /**
     * 退款单号
     */
    private String refundId;

    /**
     * 卖家id
     */
    private Long sellerId;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * 外部门店id(即商户门店id)
     */
    private String outStoreId;

    /**
     * 订单号
     */
    private String tradeId;

    /**
     * 退款状态
     */
    private String status;

    /**
     * 创建时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String createdTime;

    /**
     * 修改时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String modifiedTime;

    /**
     * 完成退款时间，格式 yyyy-MM-dd HH:mm:ss
     */
    private String refundedTime;

    /**
     * 退款原因
     */
    private String refundReason;

    /**
     * 退款商品详情
     */
    private List<RefundItemResult> refundItems;

    /**
     * 退款支付详情
     */
    private List<RefundPaymentResult> refundPayments;

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getOutStoreId() {
        return outStoreId;
    }

    public void setOutStoreId(String outStoreId) {
        this.outStoreId = outStoreId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getRefundedTime() {
        return refundedTime;
    }

    public void setRefundedTime(String refundedTime) {
        this.refundedTime = refundedTime;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public List<RefundItemResult> getRefundItems() {
        return refundItems;
    }

    public void setRefundItems(List<RefundItemResult> refundItems) {
        this.refundItems = refundItems;
    }

    public List<RefundPaymentResult> getRefundPayments() {
        return refundPayments;
    }

    public void setRefundPayments(List<RefundPaymentResult> refundPayments) {
        this.refundPayments = refundPayments;
    }
}
