package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class QuerySpuByIdResult extends BaseResult {
    private static final long serialVersionUID = -8335506418336933895L;

    private ItemSpuResult model;


    public ItemSpuResult getModel() {
        return model;
    }

    public void setModel(ItemSpuResult model) {
        this.model = model;
    }
}
