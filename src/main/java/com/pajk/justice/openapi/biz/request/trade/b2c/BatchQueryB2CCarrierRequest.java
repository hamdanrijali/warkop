package com.pajk.justice.openapi.biz.request.trade.b2c;

import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.BatchQueryB2CCarrierResult;
import com.pajk.justice.openapi.biz.request.IRequest;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class BatchQueryB2CCarrierRequest implements IRequest<BatchQueryB2CCarrierResult> {

    String apiGroup = "shennong";
    String apiName = "batchQueryB2CCarrier";
    String apiId = "2810e9c32c2174278ea5920c35608873";


    public BatchQueryB2CCarrierRequest() {

    }


    @Override
    public List<?> getData() {
        return new LinkedList<>();
    }

    @Override
    public String getApiId() {
        return apiId;
    }

    @Override
    public String getApiName() {
        return apiName;
    }

    @Override
    public String getApiGroup() {
        return apiGroup;
    }

    @Override
    public Class<BatchQueryB2CCarrierResult> getClassName() {
        return BatchQueryB2CCarrierResult.class;
    }


}
