package com.pajk.justice.openapi.biz.model.reuslt.item;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class CategoryDTO extends BaseDO {

    private static final long serialVersionUID = 1333376671950L;
    /**  CategoryId  */
    private Long id;
    /**  类目名 */
    private String name;
    /**  是否是标品类目    */
    private Boolean isCspu;

    /** 类目级别*/
    private Integer level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCspu() {
        return isCspu;
    }

    public void setCspu(Boolean cspu) {
        isCspu = cspu;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
