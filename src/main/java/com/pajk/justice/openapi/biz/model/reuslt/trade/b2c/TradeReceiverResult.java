package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by lizhijun on 2018/5/17.
 */
public class TradeReceiverResult extends BaseDO{
    private static final long serialVersionUID = 1904075534881000101L;

    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 收货人手机号
     */
    private String mobile;
    /**
     * 收货人地址
     */
    private String address;
    /**
     * 收货人地址所在身份
     */
    private String prov;
    /**
     * 收货人地址所在城市
     */
    private String city;
    /**
     * 收货人地址所在区域
     */
    private String area;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
