package com.pajk.justice.openapi.biz.act.request;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author DENGCHUHUA127 on 2018/11/26.
 * @since v1.0.0
 */
public class UserBaseRequest extends BaseDO {

    private static final long serialVersionUID = -7327638856483947733L;
    protected long userId;


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }


}
