package com.pajk.justice.openapi.biz.model.reuslt.item;

import com.pajk.justice.openapi.biz.model.reuslt.base.BaseResult;

/**
 * Created by fanhuafeng on 18/4/30.
 * Modify by fanhuafeng on 18/4/30
 */
public class ListSkuResult extends BaseResult{
    private static final long serialVersionUID = 8882292215251809984L;

    public ListSkuResult() {
    }

    public ListSkuResult(int code, String msg) {
        super(code, msg);
    }
}
