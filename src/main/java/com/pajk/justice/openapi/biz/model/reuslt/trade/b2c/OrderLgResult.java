package com.pajk.justice.openapi.biz.model.reuslt.trade.b2c;


import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * Created by fanhuafeng on 18/4/26.
 * Modify by fanhuafeng on 18/4/26
 */
public class OrderLgResult extends BaseDO {

    private static final long serialVersionUID = -7304713318859825120L;
    /**
     * 收货人昵称
     */
    private String fullName;
    /**
     * 收货人电话
     */
    private String mobilePhone;
    /**
     * 地址详情(除省市区以外信息)
     */
    private String address;
    /**
     * 省
     */
    private String prov;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;


    public String getProv() {
        return this.prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }


    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobilePhone() {
        return this.mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }


}
