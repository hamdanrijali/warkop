package com.pajk.justice.openapi.biz.model.reuslt.base;

import com.pajk.justice.openapi.biz.model.base.BaseDO;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class BaseRequest extends BaseDO{

    protected Long sellerId;

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
}
