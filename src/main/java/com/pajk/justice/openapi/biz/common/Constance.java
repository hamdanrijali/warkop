package com.pajk.justice.openapi.biz.common;

/**
 * @author dengchuhua127 on 2018/10/18.
 * @since v2.3.0
 */
public class Constance {

    public static final String APIGROUP_SHENNONG = "shennong";
    public static final String APIGROUP_RUMEX = "rumex";

}
