package com.gdt.health.openapi.controller;

import com.pajk.justice.openapi.biz.common.JkClient;
import com.pajk.justice.openapi.biz.common.QueryEnv;
import com.pajk.justice.openapi.biz.model.reuslt.item.AddSkuResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.b2c.QueryTradeDetailResult;
import com.pajk.justice.openapi.biz.model.reuslt.trade.o2o.QueryOrderResult;
import com.pajk.justice.openapi.biz.request.item.AddSkuRequest;
import com.pajk.justice.openapi.biz.request.trade.b2c.QueryTradeDetailRequest;
import com.pajk.justice.openapi.biz.request.trade.o2o.QueryOrderRequest;
import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.SyndFeedOutput;
import com.rometools.rome.io.XmlReader;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

@RestController
public class TestController {

    // K24
    // sellerid = 2238150616640607
    // tradeId = 2881969210901
    // parnterid = 24
    // key = 354907518e73d2b48bb05ff0028a537a

    // Sample test
    // partnerId = yaofang_test_01
    // key = 0031ae2efaacd0fe644580de630f9d13

    private final static Long SELLER_ID = 2238150616640607L;
    private final static String TRADE_ID = "2881969210901";
    private final static String PARTNER_ID = "24";
    private final static String KEY = "354907518e73d2b48bb05ff0028a537a";
    private final static QueryEnv ENV = QueryEnv.PROD;

    @PostMapping("/trade/detail/")
    public ResponseEntity<QueryTradeDetailResult> queryTradeDetail(@RequestBody QueryTradeDetailRequest queryTradeDetailRequest) {

        //testQueryDetail
        JkClient jkClient = new JkClient(PARTNER_ID, KEY, ENV);

        QueryTradeDetailResult result = jkClient.execute(queryTradeDetailRequest);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/sku/add/")
    public ResponseEntity<AddSkuResult> testApi(@RequestBody QueryTradeDetailRequest queryTradeDetailRequest) {

        JkClient jkClient = new JkClient(PARTNER_ID, KEY, ENV);
        AddSkuRequest request = new AddSkuRequest();
        request.setSellerId(SELLER_ID);
        request.setPrice(110L);
        request.setOrigPrice(100L);//原价必须大于price
        request.setSkuOuterId(RandomStringUtils.randomNumeric(8));
        request.setSpuId(1L);
        request.setLevel1Spec("DESCRIPTION");
        request.setLevel2Spec("COMPOSITION");
        request.setLevel3Spec("INDICATION");
        request.setLevel4Spec("DOSAGE");
        request.setLevel5Spec("SERVING");
        request.setSupportShopCard(true);
        request.setWeight(RandomStringUtils.randomNumeric(3));
        request.setPictures("T1jYKbB4hl1RCvBVdK.jpg");
        AddSkuResult result = jkClient.execute(request);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/query/order/")
    public ResponseEntity<QueryOrderResult> queryOrder() {
        QueryOrderRequest request = new QueryOrderRequest();
        request.setSellerId(SELLER_ID);
        request.setTradeId(TRADE_ID);
        JkClient jkClient = new JkClient(PARTNER_ID, KEY, ENV);
        QueryOrderResult result = jkClient.execute(request);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/rss/feed/")
    public String rssFeed() throws IOException, FeedException {
        SyndFeedInput input = new SyndFeedInput();

        URL feedSource = new URL("https://www.guesehat.com/feed");
        SyndFeed feed = input.build(new XmlReader(feedSource));


        return feed.toString();
    }
}
